/*
 * Copyright (c) 2013 Bucknell University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: L. Felipe Perrone (perrone@bucknell.edu)
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h> 
#include <unistd.h>
#include <pthread.h>

#include "circular-list.h" 

/* SCALE_FACTOR is a constant for you to experiment with:
 * if you choose a very large SCALE_FACTOR, your threads
 * might spend a long time sleeping. If you choose it to be
 * too small, your threads will not sleep at all. Note
 * that in the producer and consumer functions, the sleep
 * time is computed as the INTEGER DIVISION below:
 *
 *  usleep(SCALE_FACTOR * rand_r(&seed) / RAND_MAX
 *
 *  where RAND_MAX is the largest random numver returned
 *  by rand_r. If the numerator is smaller than RAND_MAX,
 *  the quotient of the integer division is ZERO!
 */
#define SCALE_FACTOR 1000

// global variables -----------------------

struct circular_list mylist;

// end of global variables ----------------

void *producer (void *param) {
  item i;
  unsigned int seed = 1234;

  while (true) {
    int random_val = rand_r(&seed);
    // sleep for random period of time
    usleep(SCALE_FACTOR * random_val / RAND_MAX); 
    
    // generate a random number
    int random_val2 = rand_r(&seed);
    i = (item) (((double) random_val2) / RAND_MAX);

    if (circular_list_insert(&mylist, i) == -1) {
      printf("PRODUCER: error condition\n");
    } else {
      printf("PRODUCER: produced value %lf\n", i);
    }
    //printf("Random values generated: random_val = %d, random_val2 = %d\n", random_val, random_val2);
  }
}

void *consumer (void *param) {
  item i;
  unsigned int seed = 999;

  while (true) {
    int random_val = rand_r(&seed);
    // sleep for random period of time
    usleep(SCALE_FACTOR * random_val / RAND_MAX);

    if (circular_list_remove(&mylist, &i) == -1) {
      printf("CONSUMER: error condition\n");
    } else {
      printf("CONSUMER: consumed value %lf\n", i);
    }
    //printf("Random value generated: random_val = %d\n", random_val);
  }
}

int main (int argc, char *argv[]) {

  // get command line arguments
    int num_prod, num_cons, sleep_time;

    num_prod = atoi(argv[1]);
    num_cons = atoi(argv[2]);
    sleep_time = atoi(argv[3]);

    
  // if error in command line argument usage, terminate with helpful
  // message
  if (argc != 4)
  {
        printf("Error: function only takes three arguments in this order:\n\n the number of producer threads\n the number of consumer threads \n the number of seconds to sleep before termination. \n\n please enter in three integer values.");
        exit(-1);
  }
  
  // initialize buffer
  circular_list_create(&mylist, 20);
  
  // create producer thread(s)
  pthread_t p_threads[num_prod];
  for(long thread = 0; thread<num_prod; thread++)
  {
      pthread_create(&p_threads[thread], NULL, producer, (void *) thread);
  }
  // create consumer thread(s)
  pthread_t c_threads[num_cons];
  for(long thread = 0;thread<num_cons;thread++)
  {
      pthread_create(&c_threads[thread], NULL, consumer, (void *) thread);
  }
  // sleep to give time for threads to run
  usleep(sleep_time);
  pthread_exit(NULL);
  // exit
  return (0);
}
