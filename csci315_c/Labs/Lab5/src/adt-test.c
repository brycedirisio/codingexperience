/* Bryce DiRisio
 * CSCI 315 Lab 5
 * Prof. Fuchsberger
 * 2018-9-25
 * adt-test.c
 */

#include <stdlib.h>
#include <stdio.h>
#include "circular-list.h"

int main()
    {
        struct circular_list list;
        circular_list_create(&list, 5);
        char a = 1;
        char b = 2;
        char c = 3;
        char d = 4;
        item iremove;

        circular_list_insert(&list, a);
        printf("First insertion worked\n");
        circular_list_insert(&list, b);
        printf("Second insertion worked\n");
        circular_list_insert(&list, c);
        printf("Third insertion worked\n");
        circular_list_insert(&list, d);
        printf("Fourth insertion worked\n");
        printf("Now removing elements...\n");
        printf("Removing element one...\n");
        circular_list_remove(&list, &iremove);
        printf("Removing element two... \n");
        circular_list_remove(&list, &iremove);
        printf("Removing element three... \n");
        circular_list_remove(&list, &iremove);
        printf("Removing last element... \n");
        circular_list_remove(&list, &iremove);
        printf("No errors! All working.\n");
        printf("Now lets error. Removing an element from empty list...");
        int temp = circular_list_remove(&list,&iremove);
        printf("Should return -1... let's see...\n");
        printf("%d\n", temp);

        return 0;
    }


