/*
 * Copyright (c) 2013 Bucknell University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: L. Felipe Perrone (perrone@bucknell.edu)
 */

#include <stdlib.h>
#include "circular-list.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>

pthread_mutex_t mutex;
sem_t semaphore1;
sem_t semaphore2;

int circular_list_create(struct circular_list *l, int size) {
  l->buffer = calloc(size, sizeof(item));
  l->start = -1;
  l->end = -1;
  l->elems = 0;
  l->size = size;

  //in case it errors and we can't make it for some reason...
  if(pthread_mutex_init(&mutex, NULL) == -1 || sem_init(&semaphore1, 0, size ) == -1 || sem_init(&semaphore2, 0, 0) == -1)
  {
      printf("Mutex or semaphore initialization has gone wrong.\n");
      exit(-1);
  }
  return 0;
}

int circular_list_insert(struct circular_list *l, item i) {
    sem_wait(&semaphore1);
    pthread_mutex_lock(&mutex);

    //if the array is full, return an error
    if(l->elems == l-> size){
    return -1;
    }
    if(l->start == -1)
    {
        l->start = l->start + 1;
    }
    l-> end = (l-> end + 1) % l->size;
    l->buffer[l->start] = i;
    l->elems = l->elems + 1;

    pthread_mutex_unlock(&mutex);
    sem_post(&semaphore2);

    return 0;
}

int circular_list_remove(struct circular_list *l, item *i) {
    //If array is empty, return an error.
    sem_wait(&semaphore2);
    pthread_mutex_lock(&mutex);
    if(l->elems == 0){
        return -1;
    }
    l-> start = (l->start + 1) % l->size;
    *i = l->buffer[l->end];
    l->elems = l->elems - 1;
    
    pthread_mutex_unlock(&mutex);
    sem_post(&semaphore1);
    
    return 0;
}
