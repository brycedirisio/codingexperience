/* Bryce DiRisio
 * CSCI 315 Lab 4
 * Prof. Fuchsberger
 * 2018-9-23
 * wrappers.c
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "wrappers.h"
#include <sys/wait.h>

pid_t Fork(void)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        return 0;
    }
    else
    {
        perror("error in fork");
        exit(-1);
    }
}

int Pipe(int pipefd[2])
{
    int val = pipe(pipefd);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in pipe");
        exit(-1);
    }
}

pid_t Wait(int *status)
{
    pid_t val = wait(status);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in wait");
        exit(-1);
    }
}

pid_t Waitpid(pid_t pid, int *status, int options)
{
    pid_t val = waitpid(pid, status, options);
    if(val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in waitpid");
        exit(-1);
    }
}

int Open(const char *pathname, int flags)
{
    int val = open(pathname, flags);
    if (val != -1)
    {
        return val;
    }
    else
    {
        perror("error in open");
        exit(-1);
    }
}

int Close(int fd)
{
    int val = close(fd);
    if(val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in closing pipe");
        exit(-1);
    }
}

ssize_t Write(int fd, const void *buf, size_t count)
{

    ssize_t val = write(fd, buf, count);
    if(val < 0)
    {
        perror("error in writing");
        exit(-1);
    }
    else
    {
        return val;
    }
}

ssize_t Read(int fd, void *buf, size_t count)
{
    ssize_t val = read(fd, buf, count);
    if(val < 0)
    {
        perror("error in reading");
        exit(-1);
    }
    else
    {
        return val;
    }
}

int Connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    int val = connect(sockfd, addr, addrlen);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in connecting pipe");
        exit(-1);
    }
}

int Bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    int val = bind(sockfd, addr, addrlen);

    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("Error in binding pipe");
        exit(-1);
    }
}

int Listen(int sockfd, int backlog)
{
    int val = listen(sockfd, backlog);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in listening to pipe");
        exit(-1);
    }
}

int Accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
    int val = accept(sockfd, addr, addrlen);
    if(val >= 0)
    {
        return val;
    }
    else
    {
        perror("error in accepting socket");
        exit(-1);
    }
}
