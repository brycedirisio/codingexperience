Bryce DiRisio
CSCI 315 Lab 4
Prof. Fuchsberger
2018-9-16

(P1): socket(2) "creates an endpoint" in order for the socket to effectively communicate between
programs, and returns a descriptor. It is similar to both open(2) and pipe(2) calls because all
programs in question return a file descriptor, and all calls rely on bytestream communication.
However, these calls differ in that pipes exist solely in specific hosts and connect process
outputs and inputs, whereas sockets exchange packets of information. Socket can exchange information
on a network whereas pipes and open calls are only on the localhost.

(P2):
1) They communicate via the sockaddr struct. This retains information about the IP address and address family.
2) The bind(2) call binds the thing in question to a socket, and let the socket take in input from a specified port.
3) The connect(2) call connects the sockfd to a specific address. This helps the thing in question find the correct place to connect and deliver its output.
4) The listen(2) call makes the socket in question not produce anything out, and take input from incoming connections
5) The accept(2) call essentially does the opposite of listen(2) - it will take a socket and mark it such that it will only accept incoming connections.
6) We can use the send and recieve calls as follows: send(2) can pass a message/input into a socket, then the recieve(2) call can recieve said input from the same socket.
