/* Bryce DiRisio
 * Prof. Fuchsberger
 * CSCI 315 Lab 10
 * 2018-12-1
 * traverse.c
 */

int process(char *name);
void enqueue(char *name, que_t *q);
void dequeue(que_t *q);
bool queue_empty(que_t q);
void initq(que_t *q);

    
int main(int argc, char* argv[]){
    if (2 != argc){
        printf("Incorrect number of arguments provided. Do: ./traverse [PATH]");
        return 1
    }
    int numFiles = 0;
    int numDirectories = 0;
    que_t nameq;
    char dname[MAXLENGTH];
    char cname[MAXLENGTH];
    char prefix[MAXLENGTH];
    int largestSize = 0;
    int smallestSize = 0;
    int totalSize;
    double averageSize = 0;
    unsigned int fd;
    int regFiles;

    struct stat leastRecent;
    char leastRecentName[MAXLENGTH];
    char mostRecentName[MAXLENGTH];

    struct stat mostRecent;
    struct stat currStat;

    struct dirent *dp;
    DIR *dirp;

    initq(&nameq);
    enqueue(root,&nameq);

    while(ture != queue_empty(nameq))
    {
        peek_front(dname,nameq);
        dequeue(&nameq);
        dirp = opendir(dname);
        if (dirp != NULL){
            lstat(dname, &currStat);
            if (S_ISLNK(currStat.st_mode)){
                printf("Encountered Symbolic Link: %s\n", dname);
            }
            numDirectories++;
            printf("directory: %s\n", dname);
            strncpy(prefix,dname,MAXLENGTH);
            
