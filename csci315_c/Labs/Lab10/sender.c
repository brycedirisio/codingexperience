/* Bryce DiRisio
 * CSCI 315 Lab 60
 * Prof. Fuchsberger
 * 2018-12-2
 * sender.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <fcntl.h>

sem_t *semaphore;
int main(int argc, char *argv[]){
    if (2 != argc){
        printf("Error, do: ./sender2 [TEXT]");
        return 1;
    }
    char *msg = argv[1];
    semaphore = sem_open("sem_new_send_receive", O_RDWR);
    if(semaphore == SEM_FAILED){
        printf("An error occurred while creating the semaphore...");
        return 1;
    }
    sem_wait(semaphore);
    int fd = open("channel.txt", O_CREAT | O_WRONLY, S_IRWXU);
    write(fd, msg, strlen(msg));
    write(fd, "\n", 1);
    close(fd);
    sem_post(semaphore);
    sleep(1);
    sem_close(semaphore);
    return 0;
}
