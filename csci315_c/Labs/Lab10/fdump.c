/* Bryce DiRisio
 * CSCI 315 Lab 10
 * Prof. Fuchsberger
 * 2018-11-26
 * fdump.c
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "hexdump.h"

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        printf("Incorrect amount of arguments provided. Do: fdump (file name) (offset) (size)\n");
        return 1;
    }
    
    char* name = argv[1];
    unsigned long int offset = atoi(argv[2]);
    unsigned long int size = atoi(argv[3]);

    FILE* file = fopen(name, "r"); //opens a file to read
    fseek(file, offset, SEEK_SET);

    char buf[size + 1];
    fread((void*) buf, size, 1, file);
    hexdump(buf, size);
    printf("\n");
    return 0;
}

