/* Bryce DiRisio
 * CSCI 315 Lab 2
 * 2018-9-2
 * pipe-test.c
 */

#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFFER_SIZE 26
#define READ_END 0
#define WRITE_END 1

//what is the EOF Character? -> "zero indicates end of file" 
//(may be -1?)

pid_t Fork(void)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        return 0;
    }
    else if (pid == -1)
    {
        perror("Failed to fork\n");
        exit(-1);
    }
}

int Pipe(int pipefd[2])
{
    int pid = pipe(pipefd);

    if (pid == 0)
    {
        return 0;
    }
    else if (pid == -1)
    {
        perror("Failed to pipe\n");
        exit(-1);
    }

int Write(int fd, const void *buf, size_t count)
{
    ssize_t ident = write(fd, buf, count);

    if(ident == -1)
    {
        perror("Failed to write\n");
        exit(-1);
    }
    else
    {
        return ident;
    }
}

int Read(int fd, void *buf, size_t count)
{
    int readvar = read(fd,buf,count);
    if(readvar == -1)
    {
        perror("Failed to read\n");
        exit(-1);
    }
    else
    {
        return readvar;
    }
}


int main(void)
{
    char write_msg[BUFFER_SIZE] = "Greetings\n";
    char read_char[BUFFER_SIZE];
    int fd[2];
    pid_t pid;

    if (pipe(fd) == -1){
        fprintf(stderr, "Pipe failed\n");
        return 1;
    }
    /* fork a child process */
    pid = fork();
    if (pid < 0) {
        fprintf(stderr, "Fork failed\n");
        return 1;
        }
    if (pid > 0){
        /* parent process*/
        int x = 0;
        close(fd[READ_END]);
        while(write_msg[x] != '\0')
        {
            Write(fd[WRITE_END], &write_msg[x], BUFFER_SIZE);
            printf("%s\n", &write_msg[x]);
            x = x + 1;
        }
        close(fd[WRITE_END]);
    }
    else{
        /* child process*/
        close(fd[WRITE_END]);
        int x = 0;
        int character = Read(fd[READ_END], &read_char[x], BUFFER_SIZE);
        int reading = Read(fd[READ_END], &read_char, BUFFER_SIZE);
        while(character != 0)
        {
            Write(1,&read_char[x],BUFFER_SIZE);
            x = x + 1;
            int reading = Read(fd[READ_END], &read_char, BUFFER_SIZE);

        }
        //Code from prelab
        printf("read %s\n", read_char);
        close(fd[READ_END]);
    }
    return 0;
}
}




