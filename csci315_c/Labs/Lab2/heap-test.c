/* Bryce DiRisio
 * 2018-9-2
 * CSCI 315 Lab 02 
 * heap-test.c
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int main(int argc, char *argv[]){
    int *size;
    int testValue;
    size = malloc(sizeof(int));
    testValue = 42;
    int pid;

    if((pid = fork()) == 0){
        //child
        printf("Child has current value of %d\n", testValue);
        testValue += 42;
        printf("Child now has current value of %d\n", testValue);
    }
    else{
        //parent
        printf("Parent has current value of %d\n", testValue);
        testValue += 10;
        printf("Parent now has current value of %d\n", testValue);
    }
    return(0);
    exit(0);
}



