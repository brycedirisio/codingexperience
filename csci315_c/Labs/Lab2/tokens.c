/* Bryce DiRisio
 * CSCI 315 Lab 2
 * 2018-9-2
 * pipe-test.c
 */

#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/wait.h>

#define BUFFER_SIZE 26
#define READ_END 0
#define WRITE_END 1

//what is the EOF Character? -> "zero indicates end of file" 
//(may be -1?)

pid_t Fork(void)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        return 0;
    }
    else if (pid == -1)
    {
        perror("Failed to fork\n");
        exit(-1);
    }
}

int Pipe(int pipefd[2])
{
    int pid = pipe(pipefd);

    if (pid == 0)
    {
        return 0;
    }
    else if (pid == -1)
    {
        perror("Failed to pipe\n");
        exit(-1);
    }

int Write(int fd, const void *buf, size_t count)
{
    ssize_t ident = write(fd, buf, count);

    if(ident == -1)
    {
        perror("Failed to write\n");
        exit(-1);
    }
    else
    {
        return ident;
    }
}

int Read(int fd, void *buf, size_t count)
{
    int readvar = read(fd,buf,count);
    if(readvar == -1)
    {
        perror("Failed to read\n");
        exit(-1);
    }
    else
    {
        return readvar;
    }
}


int main(void)
{
    char pwrite_msg[BUFFER_SIZE] = "Greetings\n";
    char cread_char[BUFFER_SIZE];
    char cwrite_msg[BUFFER_SIZE];
    char pread_char[BUFFER_SIZE];
    int fd1[2];
    int fd2[2];
    int count;
    ssize_t length = 0;
    pid_t pid;

    Pipe(fd1);
    Pipe(fd2);

    if (pipe(fd1) == -1){
        fprintf(stderr, "Pipe failed\n");
        return 1;
    }
    /* fork a child process */
    pid = Fork();
    if (pid < 0) {
        fprintf(stderr, "Fork failed\n");
        return 1;
        }
    if (pid > 0){
        /* parent process*/
        int x = 0;
        close(fd1[READ_END]);
        close(fd2[WRITE_END]);

        while(0)
        {
         printf("input line: ");
         char *input;
         input = (char *) malloc(BUFFER_SIZE + 1);
         ssize_t read = getline(&input, &length, stdin);
         strncpy(pwrite_msg, input, sizeof(input) - 1);
         int writtenchar = Write(fd1[WRITE_END], &length, sizeof(length));
         Write(fd1[WRITE_END], &pwrite_msg, sizeof(&pwrite_msg));
         close(fd1[WRITE_END]);
         wait(&count);
         close(fd2[READ_END]);
        }
        
    }
    else
    {
        /* child process*/
        close(fd1[WRITE_END]);
        close(fd2[READ_END]);
        int x = 0;
        int character = Read(fd1[READ_END], &cread_char[x], BUFFER_SIZE);
        while(character != 0)
        /*
         * code from upper.c
        {
            Write(1,&cread_char[x],BUFFER_SIZE);
            cwrite_msg[x] = toupper(cread_char[x]);
                printf("\n, new msg = %s\n", cwrite_msg);
                Write(fd2[WRITE_END], &cwrite_msg[x], BUFFER_SIZE);
                x = x + 1;
            int reading = Read(fd1[READ_END], &cread_char[x], BUFFER_SIZE);

        }
        */
        exit(0);
        printf("read %s\n", cread_char);
        close(fd1[READ_END]);
        close(fd2[WRITE_END]);
    }
    return 0;
    }
}





