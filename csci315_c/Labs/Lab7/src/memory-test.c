/*Bryce DiRisio
 * Prof. Fuchsberger
 * CSCI315 - Lab 7
 * 2018-10-31
 * memory-test.c
 */

#include "allocator.h"
#include "dnode.h"
#include "dlist.h"
#include <stdio.h>


size_t testNum[] = {123, 15, 87, 30, 200, 3400,5,23,15, 99999,34};
void* testptrs[11];
char *method[] = {"FIRST_FIT","BEST_FIT","WORST_FIT"};
int main(int argv, char *argc[]){
  size_t set_size = 200000;
  int init;
  void *ptr;
  printf("Initializing the allocator\n");
  init = allocator_init(set_size);
    for(int i = 0; i < 11; i++)
    {
      printf("Allocating block of size %zu using %s\n", testNum[i], method[0]);
      ptr = allocate(testNum[i]);
      if (ptr != NULL)
      {
	testptrs[i] = ptr;
	printf("Allocation sucessful\n");
      }
    }
    printf("Deallocating pointer to nodes of size 1234\n");
    int a = deallocate(testptrs[0]);
    if (a == 0){
      printf("successfully deallocated pointer of size %zu\n");
    }
  return 0;
}

