/*Bryce DiRisio
 * CSCI 315 Lab 7
 * Professor Fuchsberger
 * 2018-10-23
 * allocator.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "dnode.h"
#include "dlist.h"
#include "allocator.h"

int allocator_init(size_t size)
{   
    memory = malloc(size);
    if (memory == NULL)
    {
        return -1;
    }

    struct dlist *free_list = dnode_create();
    struct dlist *allocate_list = dnode_create();
    
    dlist_add_back(free_list, NULL);
    struct dnode* begin = free_list->front;
    begin -> data = memory;
    begin -> size = size;

    return 0;
    
}

void *allocate(size_t size)
{
    if (method == "FIRST_FIT")
    {
        return allocate_first_fit(size);
    }
    else if (method == "BEST_FIT")
    {
        return allocate_best_fit(size);
    }
    else
    {
        return allocate_worst_fit(size);
    }
}

int deallocate(void *ptr)
{
    struct dnode *n = allocate_list -> front;
    while((n != NULL) && (n->data != ptr))
    {
        n = n-> next;
    }
    
    struct dnode* remove = n
    int size = remove -> size;

    dlist_add_back(free_list, ptr);
    struct dnode* back = free_list->back;
    back -> size = size;
    
    dlist_find_remove(allocate_list, ptr);

    return 0;
}

void* allocate_first_fit(size_t size)
{
    struct dnode* first = free_list->front;
    if (first == NULL)
    {
        return NULL;
    }
    do
    {
        if(first -> size >= size)
        {
            dlist_add_back(allocate_list, first->data);
            struct dnode* end = allocate_list->back;
            end->size = size;
            temp -> size -= size;
        }
        return end-> data;
    }
    while((first = first-> next) != NULL);
    
    return NULL;
}

void* allocate_best_fit(size_t size)
{
    struct dnode* front = dlist_get_front_node(free_list);
    int best_size = INT_MAX;
    struct dnode* best = NULL

    /* base case - nowhere to allocate to*/
    if (front == NULL)
    {
        return NULL;
    }

    /* get the best place to put the program*/
    do
    {
        if ((front->size < best_size) && (front -> size >= size))
        {
            best_size = front->size;
            best_fit = front;
            if(front->size == size)
            {
                break;
            }
        }
    }
    while ((front = front->next) != NULL)

    /*let's allocate the data*/
    if (best_fit != NULL)
    {
        dlist_add_back(allocate_list, best_fit -> data);
        struct dnode* end = dlist_get_back_node(allocate_list);
        end->size = size;

        best_fit -> size -= size;

        if (best_fit -> size == 0)
        {
            dlist_find_remove(free_list, best_fit -> data);
        }
        else
        {
            best_fit -> data = (void*) ((intptr_t)(best_fit->data)+size);
        }

        return end -> data;
    }
    return NULL;
}

void* allocate_worst_fit(size_t size)
{
    struct dnode* worst = dlist_get_front_node(free_list);
    int worst_size = 0;
    struct dnode* worst_fit = NULL;

    if (worst == NULL)
    {
        return -1;
    }
    do
    {
        if ((worst->size > worst_size) &&(worst -> size >=size))
        {
            worst_size = worst-> size;
            worst_size = worst;
            if(worst->size == size) 
            {
                break;
        }
    }
    while((worst = worst-> next) != NULL);

    if (worst_fit != NULL)
    {
        dlist_add_back(allocate_list, worst_fit-> data);
        struct dnode* end = dlist_get_back_node(allocate_list);
        end -> size = size;
        worst_fit-> size -= size;
        if(worst_fit -> size == 0)
        {
            dlist_find_remove(free_list, worst_fit->data);
        }
        else
        {
            worst_fit -> data = (void*) ((intptr_t) (worst_fit ->data) + size);
        }
        return end->data;
    }
    return -1;
}

