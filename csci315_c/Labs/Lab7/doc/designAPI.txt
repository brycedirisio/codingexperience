Bryce DiRisio
Prof. Fuchsberger
CSCI 315 Lab 7
2018-10-28
designAPI.txt

First Fit:
    - Traverses the linked list until it finds a node that's large enough to handle the request/encapsulate the size inputted.

Best Fit:
    - Traverses the linked list until it finds the smallest node that can fit the request/encapsulate the size inputted.

Worst Fit:
    -Traverses the linked list until it finds the largest node that can fint the request/encapsulate the size inputted.

all testing done under memory-test.c

dlist.c:
    -the dlist_print function helps to keep track of the memory when the test functions run.

dnode.h:
    -size attribute added in order to keep track of how big the memory/linked list gets
