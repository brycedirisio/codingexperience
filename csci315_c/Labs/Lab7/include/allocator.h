/* Bryce DiRisio
 * CSCI 315 Lab 7
 * Professor Fuchsberger
 * 2018-10-23
 * allocator.h
 */

#include <stddef.h>

int allocator_init(size_t size);

/**
 * Initializes two doubly-linked lists to manage memory. Linked 
 * lists will keep track of memory available and memory being 
 * used. 
 */

void *allocate(size_t size);
/**
 * Allocates a block of memory of an inputted size according to
 * multiple allocation policies. It's functionality is modifying
 * and saving the modified block of memory into the allocated_list
 * linked list.
 */

int deallocate(void *ptr);
/**
 * Frees a block of memory from being "used" by a program. 
 * returns 0 on a successful call. Does the opposite  of 
 * the allocate call above.
 */
void *first_fit(size_t size);
/**
 * Finds the first available slot in memory to save the
 * program data into. Algorithm type for allocating memory.
 */
void *best_fit(size_t size);
/**
 * Finds the slot in memory which leads to the least leftover
 * excess memory when memory is allocated to a program. Algorithm
 * type for allocating memory.
 */
void *worst_fit(size_t size);
/**
 * Finds the slot in memory which leads to the most leftover
 * space in memory when memory is allocated to a program. 
 * Algorithm type for allocating memory.
 */
