/* Bryce DiRisio
 * CSCI 315 - preLab 01
 * 2018-8-27
 * Prof. Perrone 
 * myprog.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char *argv[]){

    int parentCounter = 0;
    int childCounter1 = 0;
    int childCounter2 = 0;

    int net_val = 0;
    net_val = fork();
    if(0==net_val){
    }
    else{
        int child_val = fork();
    }
    while(1){
        printf("parent: %d/n", parentCounter);
        parentCounter = parentCounter + 1;
        if((childCounter1 = fork()) == 0){
            printf("child1: %d\n", childCounter1);
            childCounter1 = childCounter1 + 1;
            }
        else if((childCounter2 = fork()) == 0){
            printf("child2: %d\n", childCounter2);
            childCounter2 = childCounter2 + 1;
            }
        }

    return 0;
}
