/* Bryce DiRisio
 * CSCI 315 - preLab 01
 * 2018-8-27
 * Prof. Perrone
 * cmdreverse.c
 */

#include <stdio.h>
#include <string.h>

int main( int argc, char *argv[]){
    char *string = argv[1];
    int front = 0;
    int back = strlen(string) - 1;
    if (argc != 2){
        printf("Argument Error: Invalid number of argument entries");
        }
    else{
        while(front<back){
            char firstChar = string[front];
            string[front] = string[back];
            string[back] = firstChar;
            front = front + 1;
            back = back - 1;
        }
        printf("%s/n",string);
        }
    return 0;
}
    

