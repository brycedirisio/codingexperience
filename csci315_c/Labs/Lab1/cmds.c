/*Bryce DiRisio
 * CSCI 315 - preLab 01
 * 2018-8-27
 * Prof. Perrone
 * cmds.c
 */

#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[]){
    //character
    char *c = argv[1];
    //integer
    int i = atoi(argv[2]);
    //float
    float f = atof(argv[3]);
    //string
    char *s = argv[4];

    if(argc != 5){
        printf("ARGUMENT ERROR: Invalid number of arguments provided");
    }
    else{
        printf("Here's what you typed:\n%s\n%d\n%f\n%s",c,i,f,s);
    }
    return 0;
}

