/* Bryce DiRisio
 * CSCI315 Lab
 * 2018-10-14
 * Professor Fuchsberger
 * dp.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

int napping(int t)
{
    t = t/(RAND_MAX /1000000 ); //convert to micro
    usleep(t);
}

void* philosopher(void* param)
{
    int id = (intptr_t) param;
    unsigned int seed = id + 1;

    while(1)
    {
        printf("Philosopher %d is thinking.\n", id);
        int t =rand_r(&seed);
        napping(t);

        printf("Philosopher %d is hungry.\n", id);
        printf("Philosopher %d is starting to eat.\n", id);
        t = rand_r(&seed);
        napping(t);

        printf("Philosopher %d is done eating.\n", id);
    }
}

int main()
{
    pthread_t tid[5];
    int num;
    for (num=0;num<5; num++)
    {
        pthread_create(&tid[num],NULL, philosopher, (void*)(intptr_t) num);
    }

    sleep(10);
    return 0;
}

