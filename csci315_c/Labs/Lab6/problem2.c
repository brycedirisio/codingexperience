/* Bryce DiRisio
 * CSCI315 Lab
 * 2018-10-22
 * Professor Fuchsberger
 * problem2.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>

pthread_mutex_t chopsticks[5];

int napping(int t)
{
    t = t/(RAND_MAX /1000000 );
    usleep(t);
}

void* philosopher(void* param)
{
    int id = (intptr_t) param;
    unsigned int seed = id + 1;

    while(1)
    {
        printf("Philosopher %d is thinking.\n", id);
        fflush(stdout);
        int t=rand_r(&seed);
        napping(t);

        printf("Philosopher %d is hungry.\n", id);
        fflush(stdout);
        pthread_mutex_lock(&chopsticks[id]);
        printf("Philosopher %d took the left chopstick.\n", id);
        fflush(stdout);
        napping(t);
        pthread_mutex_lock(&chopsticks[(id+1)%5]);
        printf("Philosopher %d took the right chopstick.\n", id);
        fflush(stdout);

        printf("Philosopher %d is starting to eat.\n", id);
        fflush(stdout);
        t = rand_r(&seed);
        napping(t);
        printf("Philosopher %d is done eating.\n", id);
        fflush(stdout);

        pthread_mutex_unlock(&chopsticks[id]);
        printf("Philosopher %d has dropped the left chopstick.\n", id);
        fflush(stdout);
        pthread_mutex_unlock(&chopsticks[(id+1)%5]);
        printf("Philosopher %d has dropped the right chopstick.\n", id);
        fflush(stdout);
    }
}

int main()
{
    pthread_t tid[5];
    int num;
    for (num=0;num<5; num++)
    {
        pthread_create(&tid[num],NULL, philosopher, (void*)(intptr_t) num);
    }

    sleep(20);
    return 0;
}

