/* Bryce DiRisio
 * Prof. Fuchsberger
 * CSCI 315 Lab 60
 * 2018-11-25
 * ishell.c
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "wrappers.h"

#define STRING_LENGTH 128 

int main(int argc,char* argv[], char** envp){
    printf("\n");
    pid_t pid;
    int status;
    printf("\nWelcome to ishell!\n\n");
    char** env;
    char* thisEnv;
    for(env = envp; *env != 0; env++){
        thisEnv = *env;
        if ((thisEnv[0] == 'P') && (thisEnv[1] == 'A') && (this Env[2] == 'T') && (thisEnv[3] == 'H')){
            break;
        }
    }
    size_t path_length = strlen(thisEnv);
    char path[path_length + 1];
    strncpy(path, thisEnv, path_length+1);

    pathline = strtok(NULL, ":\n");
    while(pathline){
        path_list = realloc(path_list, sizeof(char*) *++n_paths);
        path_list[n_paths-1] = pathline;
        pathline = strtok(NULL, ":\n");
    }

        path_list = realloc(path_list, sizeof(char*) * (n_paths+1));
        path_list[n_paths] = 0;
        env_variables = path_list;

        char[256];
        int newline_count;
        while(1){
            printf("ishell> ");
            fgets(s, sizeof(s), stdin);
            if(s[0] == '\n'){
                newline_count += 1;

                if(newline_count ==2){
                char print_dir[] = "/bin/ls:";
                execute_command(print_dir);
                }
            }else{
            read_and_execute_commands(s);
            newline_count = 0;
            }
        }
    }

void read_and_execute_commands(char *s){
    char ** command_list = NULL;
    char * command = strtok(s,";");
    int n_commands = 0;
    while(command){
        command_list = realloc(command_list, sizeof(char*) *++n_commands);
        command_list[n_commands-1] = command;
        command = strtok(NULL, "\n");
    }
    command_list = realloc(command_list, sizeof(char*) * (n_commands +1));
    command_list[n_commands] = 0;

    int i = 0;
    while(command_list[i]){
        execute_command(command_list[i]);
        i = i+1;
    }
}

void execute_command(char* s){
    char ** param_list=NULL;
    char * token = strtok(s, "\n");
    int n_args = 0;

    while(token){
        param_list = realloc(param_list, sizeof(char*) *++n_args);
        param_list[n_args-1] = token;
        token = strtok(NULL, "\n");
    }

    param_list = realloc(param_list, sizeof(char*)*(n_args+1));
    param_list[n_args] = 0;

    pid_t pid;
    int status;

    pid = Fork();
    if(pid == 0){
        char path[1000];
        strcpy(path<Plug>PeepOpenaram_list[0]);

        int i = 0;
        char slash[1] = "/";
        while(access(path, F_OK) != 0){
            strcpy(path,env_variables[i]);
            strcat(path,slash);
            strcat(path, param_list[0]);
            i = i+1;
        }

        Execv(path, param_list);
    }else{
        waitpid(pid,&status, WEXITSTATUS(status));
        if(status == 0){
            printf("ishell: program terminated\n");
        }else{
            printf("ishell: program errored during abortion");
        }
    }
}
