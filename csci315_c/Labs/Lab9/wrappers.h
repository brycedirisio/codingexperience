/*Bryce DiRisio
 * CSCI 315 Lab 4
 * Prof. Fuchsberger
 * 2018-9-23
 * wrappers.h
 */

#ifndef WRAPPERS_H
#define WRAPPERS_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

pid_t Fork(void);
int Pipe(int pipefd[2]);
pid_t Wait(int *status);
pid_t Waitpid(pid_t pid, int *status, int options);
int Open(const char *pathname, int flags);
int Close(int fd);
ssize_t Write(int fd, const void *buf, size_t count);
ssize_t Read(int fd, void *buf, size_t count);
int Connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int Bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int Listen(int sockfd, int backlog);
int Accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
void ReadXBytes(int sockfd, unsigned int x, void* buffer);
int Socket(int domain, int type, int protocol);
ssize_t Sendto(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *dest_addr, socklen_t addrlen);
ssize_t Recvfrom(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t addrlen);
int Execv(const char* path, char* const argv[]);

#endif
