/* Bryce DiRisio
 * CSCI 315 Lab 4
 * Prof. Fuchsberger
 * 2018-9-23
 * wrappers.c
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "wrappers.h"
#include <sys/wait.h>

pid_t Fork(void)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        return 0;
    }
    else
    {
        perror("error in fork");
        exit(-1);
    }
}

int Pipe(int pipefd[2])
{
    int val = pipe(pipefd);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in pipe");
        exit(-1);
    }
}

pid_t Wait(int *status)
{
    pid_t val = wait(status);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in wait");
        exit(-1);
    }
}

pid_t Waitpid(pid_t pid, int *status, int options)
{
    pid_t val = waitpid(pid, status, options);
    if(val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in waitpid");
        exit(-1);
    }
}

int Open(const char *pathname, int flags)
{
    int val = open(pathname, flags);
    if (val != -1)
    {
        return val;
    }
    else
    {
        perror("error in open");
        exit(-1);
    }
}

int Close(int fd)
{
    int val = close(fd);
    if(val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in closing pipe");
        exit(-1);
    }
}

ssize_t Write(int fd, const void *buf, size_t count)
{

    ssize_t val = write(fd, buf, count);
    if(val < 0)
    {
        perror("error in writing");
        exit(-1);
    }
    else
    {
        return val;
    }
}

ssize_t Read(int fd, void *buf, size_t count)
{
    ssize_t val = read(fd, buf, count);
    if(val < 0)
    {
        perror("error in reading");
        exit(-1);
    }
    else
    {
        return val;
    }
}

int Connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    int val = connect(sockfd, addr, addrlen);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in connecting pipe");
        exit(-1);
    }
}

int Bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    int val = bind(sockfd, addr, addrlen);

    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("Error in binding pipe");
        exit(-1);
    }
}

int Listen(int sockfd, int backlog)
{
    int val = listen(sockfd, backlog);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        perror("error in listening to pipe");
        exit(-1);
    }
}

int Accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
    int val = accept(sockfd, addr, addrlen);
    if(val >= 0)
    {
        return val;
    }
    else
    {
        perror("error in accepting socket");
        exit(-1);
    }
}

void ReadXBytes(int sockfd, unsigned int x, void* buffer){
    int bytesRead = 0;
    int val;
    while(bytesRead < x){
        val = read(sockfd, buffer+bytesRead, x-bytesRead);
        if(val<1)
        {
            char error_mes[] = "Read error\n";
            perror(error_mes);
            exit(-1);
        }
        bytesRead += val;
    }
}

int Socket(int domain, int type, int protocol){
    int val = socket(domain, type, protocol);
    if(val >= 0) return val;
    else if (val == -1){
        char error_mes[] = "Error in creating socket";
        perror(error_mes);
        return 1;
    }
}

ssize_t Sendto(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *dest_addr, socklen_t addrlen){
    int val = sendto(sockfd, buf, len, flags, dest_addr, addrlen);
    if(val = 0) return val;
    else if (val ==-1){
        char error_mes[] = "Sendto error";
        perror(error_mes);
        exit (-1);
    }
}

ssize_t Recvfrom(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen){
    int val = recvfrom(sockfd, buf, len, flags, src_addr, addrlen);
    if (val >= 0) return val;
    else if (val == -1){
        char error_mes[] = "error in Recvfrom";
        perror(error_mes);
        exit (-1);
    }
}


int Execv(const char* path, char* const argv[]){
    int val = execv(path, argv);
    if(val == -1){
        char error_mes[] = "Execv failed";
        perror(error_mes);
        return 1;
    }
}
    
