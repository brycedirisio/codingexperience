/* Bryce DiRisio
 * CSCI 315 prelab 3.1
 * Dr. Fuchsberger
 * 2018-9-10
 * char-threads.c
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


int i1 = 0;
int i2 = 0;

void *thread1(void* voidptr)
{
    while(1)
    {
        char numbers[10] = {'0','1','2','3','4','5','6','7','8','9'};
        printf("%c\n", numbers[i1]);
        i1 ++;
        if (i1 == 10)
        {
            i1 = 0;
        }
        int count = 0;
        int i;
        for(i = 1; i<1000000; i++)
        {
            count += i;
        }
        return 0;
        }
}


void *thread2(void* voidptr)
{
    while(1)
    {
        char letters[26] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        printf("%c\n", letters[i2]);
        i2++;
        if (i2 == 27)
        {
            i2 = 0;
        }
        int count = 0;
        int i;
        for(i = 1; i < 1000000; i++)
        {
            count += i;
        }   
        return 0;
    }
}

void *thread3(void* voidptr)
{
    while(1){
        printf("#\n");
        int count = 0;
        int i;
        for(i = 1; i < 1000000; i++)
        {
            count += i;
        }
        return 0;
    }
}

int main(void)
{
    //initialize the threads 
    pthread_t tid[3];
    while(1)
    {
        pthread_create(&tid[0],NULL, thread1, NULL);
        pthread_create(&tid[1],NULL, thread2, NULL);
        pthread_create(&tid[2],NULL, thread3, NULL);
        pthread_join(tid[0], NULL);
        pthread_join(tid[1], NULL);
        pthread_join(tid[2], NULL);
    }
    exit(0);
}






