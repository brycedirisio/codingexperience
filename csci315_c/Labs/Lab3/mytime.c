/* Bryce DiRisio
 * CSCI 315 prelab 3.2
 * Dr. Fuchsberger
 * 2018-9-10
 * mytime.c
 */

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

int main(void)
{
    struct timeval time;
    gettimeofday(&time,0);

    time_t currentTime = time.tv_sec;
    char* moddedTime = ctime(&currentTime);
    printf("Here is the resulting string: %s\n", moddedTime);
    return 0;
}
