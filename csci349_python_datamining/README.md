# csci349_2020sp

Student: Bryce DiRisio

Professor: Brian King

Location: Breakiron 066

Time: Monday Wednesday and Friday, 2:00 - 2:52 PM

Course: CSCI 349 01 - Intro to Data Mining



Programming Skills: Machine Learning, Data Analysis, Object-Oriented Programming
Language Skills: Python, Java, Javascript, C, C++, Verilog

Post-graduation goals: I am hoping to be able to find a job where I can utilize the skills I learned 
over my coursework. I'm looking to work within either IT, software development, or consulting.
