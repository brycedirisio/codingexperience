color(red).
color(blue).
color(yellow).
color(green).

helperlaw([Head,Tail]) :- color(First), color(Second), Head = First, Tail = Second, Head \= Tail.

coloring([]).
coloring([Head|Tail]) :- helperlaw(Head), coloring(Tail).

/**
INPUT 1:
?- coloring( [ [Switz, France], [Switz, Italy], [Switz, Germany], [Switz, Austria], [Germany, France], [Germany, Austria], [France, Italy] ]).

OUTPUT 1:
Switz = red,
France = Austria, Austria = blue,
Italy = Germany, Germany = yellow ;

Switz = red,
France = blue,
Italy = Germany, Germany = yellow,
Austria = green ;

Switz = red,
France = Austria, Austria = blue,
Italy = yellow,
Germany = green;

...

INPUT 2:
?- coloring( [ [Penn,Maryland], [Penn, Delaware], [Penn, NewJersey], [Penn, NewYork], [Penn, Ohio], [Penn, WestVirginia], [Ohio, WestVirginia], [WestVirginia, Maryland], [Maryland, Delaware], [Delaware, NewJersey], [NewJersey, NewYork], [Virginia, WestVirginia], [Virginia, Maryland], [Virginia, Kentucky], [Kentucky, Ohio], [Kentucky, Indiana], [Indiana, Ohio] ]).

OUTPUT 2:
Penn = Virginia, Virginia = Indiana, Indiana = red,
Maryland = NewJersey, NewJersey = Ohio, Ohio = blue,
Delaware = NewYork, NewYork = WestVirginia, WestVirginia = Kentucky, Kentucky = yellow ;

Penn = Virginia, Virginia = red,
Maryland = NewJersey, NewJersey = Ohio, Ohio = blue,
Delaware = NewYork, NewYork = WestVirginia, WestVirginia = Kentucky, Kentucky = yellow,
Indiana = green ;

Penn = Virginia, Virginia = Indiana, Indiana = red,
Maryland = NewJersey, NewJersey = Ohio, Ohio = blue,
Delaware = NewYork, NewYork = WestVirginia, WestVirginia = yellow,
Kentucky = green;

...

Example of query somewhere else in the world: Canada's Provinces

INPUT 3:
?- coloring( [ [NovaScotia, Quebec],[Quebec, Newfoundland],[Quebec, Ontario],[Ontario, Manitoba],[Manitoba, Saskatchewan],[Manitoba, Nunavut],[Nunavut, NWTerritories], [Saskatchewan, Alberta], [Saskatchewan, NWTerritories], [Alberta, NWTerritories],[Alberta, BritishColombia],[NWTerritories, BritishColombia],[NWTerritories, Yukon],[Yukon, BritishColombia] ]).

OUTPUT 3:

NovaScotia = Newfoundland, Newfoundland = Ontario, Ontario = Saskatchewan, Saskatchewan = Nunavut, Nunavut = BritishColombia, BritishColombia = red,
Quebec = Manitoba, Manitoba = NWTerritories, NWTerritories = blue,
Alberta = Yukon, Yukon = yellow ;

NovaScotia = Newfoundland, Newfoundland = Ontario, Ontario = Saskatchewan, Saskatchewan = Nunavut, Nunavut = BritishColombia, BritishColombia = red,
Quebec = Manitoba, Manitoba = NWTerritories, NWTerritories = blue,
Alberta = yellow,
Yukon = green ;

NovaScotia = Newfoundland, Newfoundland = Ontario, Ontario = Saskatchewan, Saskatchewan = Nunavut, Nunavut = Yukon, Yukon = red,
Quebec = Manitoba, Manitoba = NWTerritories, NWTerritories = blue,
Alberta = yellow,
BritishColombia = green ;

...

*/
