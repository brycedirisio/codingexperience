numberOfLeaves(leaf(_),1).

numberOfLeaves(node(Tree1,Tree2), Number):- numberOfLeaves(Tree1, Leaf1),
                numberOfLeaves(Tree2, Leaf2), Number is Leaf1 + Leaf2.
        

position(leaf(X), Value, 1):- X = Value.

position(node(Tree1,Tree2),X,N) :-
            position(Tree1, X, N).

position(node(Tree1,Tree2),X,N):-
    position(Tree2,X,Leaf1),
    numberOfLeaves(Tree1,Leaf2), N is Leaf1+Leaf2.

