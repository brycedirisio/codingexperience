#Bryce DiRisio
#CSCI 208
#BashB Lab

def findAllCombos(setOfChars, combo, comboLength):
    if len(setOfChars) == 0:
        if len(combo) == comboLength:
            return [combo]
        else:
            return []
    elif len(combo) == comboLength:
        return [combo]
    else:
        firstChar = setOfChars[0]
        restOfChars = setOfChars[1:]
        rest = findAllCombos(restOfChars, combo, comboLength)
        first = findAllCombos(restOfChars, firstChar + combo, comboLength)
        return rest + first
