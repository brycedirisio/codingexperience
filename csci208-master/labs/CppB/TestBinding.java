// Needs Clown and CircusClown
// 
// In this, we see that both bob and Joejr hop up and down
// rather than twirl like a ballerina. Since hoping up and
// down was in the child class, we can say that java
// uses dynamic method binding.
public class TestBinding {

    public static void main(String[] args) {
	Clown carl = new Clown("Carl"); // Here's Carl
	// Clowns twirl
	carl.dance(); // Make Carl dance.

	CircusClown joe = new CircusClown("Joe");
	// Circus Clowns hop
	joe.dance(); // Make Joe dance.
    
        // Implicit Coercion
	Clown bob = new CircusClown("Bob");
	// Does bob twirl or hop?
	bob.dance(); 

        // Explicit Coercion
	Clown joeJr = (Clown) joe;
	// Does joeJr twirl or hop?
	joeJr.dance();
    }
}
