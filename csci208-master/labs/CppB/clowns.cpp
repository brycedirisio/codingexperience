/*Bryce DiRisio
 * CSCI 208
 * CppB Lab
 * clowns.cpp
 */

using namespace std;

#include <iostream>

class Clown{
public:
    virtual void dance() {cout<< " twirls like a ballerina" << endl<<endl;}
};
class CircusClown : public Clown{
public:
    void dance() {cout<< " hops up and down" << endl<<endl;}
};

int main(){
    Clown * Carl = new Clown();
    CircusClown * Joe = new CircusClown;
    Clown * Bob = new CircusClown;
    Clown * Joejr = Joe;
    cout<<"Carl";
    Carl->dance();
    cout<<"Joe";
    Joe->dance();
    cout<<"Bob";
    Bob->dance();
    cout<< "Joe";
    Joejr->dance();
}
