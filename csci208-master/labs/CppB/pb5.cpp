/*Bryce DiRisio
 *CSCI 208
 *CppB Lab
 *pb5.cpp
 */

#include <iostream>
using namespace std;

int main(){

    //PART 1
    int a = 1, b = 1; //Yes, a and b are normal ints
    const int c = 1; //Yes, c is a const int
    int * d = &a; //Yes, d is a normal pointer to a normal int
    //int * e = &c;  No, e is a normal pointer to a const int, and 
    const  int * f = &a; //Yes, f is a normal pointer to a normal int
    const int * g = &c; //Yes, g is a normal pointer to a const int
    int * const h = &a; //Yes, h is a const pointer to a normal int
    const int * const i = &c; //Yes, i is a const pointer to a const int
    int & j = a; //Yes, j is a reference to a normal int
    //int & k = c; No, it is illegal to reference a const int w/ a normal ref.  
    const int & m = a; //Yes, reference to a normal int
    //int & const n = a; //No, you cannot apply this const to a reference.
    
    //PART 2
    a = 2; //YES
    //c = 2; NO, c is a constant int, so you cannot change the value.
    *d = 3; //YES
    //*f = 3; NO, f is a const pointer to reference a, so you cannot change what it is pointing to.
    f = &b; //YES
    //h = &b; NO, h is a const pointer to a normal int, so it cannot be changed.
    //*j = 3; NO, indirection requires a pointer
    //m = 3; NO, m is a const reference to a normal int, so you cannot change what it is referencing.
    
}
