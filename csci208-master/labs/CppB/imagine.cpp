#include <iostream>
#include "imagine.h"
using namespace std;

Imag::Imag(){
    this->real = 1;
    this->imag = 0.5;
};

Imag::Imag(double real2, double imag2){
    this->real = real2;
    this->imag = imag2;
};

Imag Imag::add(Imag given){
    Imag ans;
    ans.real = this->real + given.real;
    ans.imag = this->imag + given.imag;
    return ans;
};

Imag Imag::mult(Imag given){
    Imag ans;
    ans.real = (this->real * given.real)-(given.imag * this->imag);
    ans.imag = (this->real * given.imag) + (given.real * this->imag);
    return ans;
}

Imag operator+(Imag n1, Imag n2){
    Imag ans = n1.add(n2);

    return ans;
};

Imag operator*(Imag n1, Imag n2){
    Imag ans = n1.mult(n2);
    return ans;
};

ostream & operator<<(ostream &c, Imag n){
    c << n.real << "+i"<<n.imag<<endl;
    return c;
};


int main(){
    Imag n1 = Imag(0.3, 0.2);
    Imag n2 = Imag(0.6, 0.75);
    cout << n1 << endl;
    cout << n2 << endl;
    cout << "n1 + n2 = " << (n1+n2) << endl;
    cout << "n1 * n2 = " << (n1*n2) << endl;
    cout << "n1 * n1 * n1 * n1 = " << (n1 * n1 * n1 * n1) << endl;
}

