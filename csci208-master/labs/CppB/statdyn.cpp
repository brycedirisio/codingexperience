//compile: g++ statdyn.cpp -o statdyn
//run: ./statdyn

#include <iostream>

using namespace std;

class A{
public:
	virtual void f() { cout << "Method from A" << endl; }
	void g() { f(); }
};

class B : public A{
public:
	void f() { cout << "Method from B" << endl; }
};

int main(){
	B b;
	b.g();
}
