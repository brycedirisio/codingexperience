/*Bryce DiRisio
 *CSCI 208
 *ProLogA
 *digraph.pl
 */
edge(a,b).
edge(a,f).
edge(f,g).
edge(f,c).
edge(g,c).
edge(c,e).
edge(f,e).
edge(e,d).
edge(b,c).
edge(c,d).

pathLen2(Node1,Node2) :- edge(Node1, SomeNode), edge(SomeNode, Node2).

pathLen3a(Node1,Node3) :- edge(Node1,FirstSomeNode), edge(FirstSomeNode,SecondSomeNode), edge(SecondSomeNode,Node3).

pathLen3b(Node1,Node3) :- pathLen2(Node1,Node2), edge(Node2, Node3).

path(Node1,Node2) :-
    Node1 = Node2.
path(Node1,Node2) :-
    edge(Node1,Node2).
path(Node1,Node2) :-
    edge(Node1,SomeNode),
    path(SomeNode,Node2).
