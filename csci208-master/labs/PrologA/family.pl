male(bryce).
male(darryl).
male(ryan).
male(chris).
male(roderick).
male(david).
male(derek).

female(grace).
female(aislyn).
female(tara).
female(geraldine).
female(beth).
female(paula).

parent(darryl,bryce).
parent(darryl,grace).
parent(darryl,aislyn).
parent(tara,bryce).
parent(tara,grace).
parent(tara,aislyn).
parent(derek,chris).
parent(derek,ryan).
parent(paula,ryan).
parent(paula,chris).
parent(roderick,darryl).
parent(roderick,derek).
parent(roderick,david).
parent(geraldine,darryl).
parent(geraldine,derek).
parent(geraldine,david).


father(Person1,Person2):- 
    male(Person1),
    parent(Person1,Person2).

mother(Person1,Person2):-
    female(Person1),
    parent(Person1,Person2).

son(Person1,Person2):-
    male(Person1),
    parent(Person2,Person1).

daughter(Person1,Person2):-
    female(Person1),
    parent(Person2,Person1).

grandparent(Person1,Person2):-
    parent(Person1,Someperson),
    parent(Someperson,Person2).

sibling(Person1,Person2):-
    parent(Someperson,Person1),
    parent(Someperson,Person2),
    Person1\=Person2.

aunt(Person1,Person2):-
    female(Person1),
    sibling(Person1,Someperson),
    parent(Someperson,Person2).

uncle(Person1,Person2):-
    male(Person1),
    sibling(Person1,Someperson),
    parent(Someperson,Person2).

cousin(Person1,Person2):-
    parent(Someperson,Person1),
    sibling(Someperson,Someotherperson),
    parent(Someotherperson,Person2).

ancestor(Person1,Person2):-
    parent(Person1,Person2).

ancestor(Person1,Person2):-
    parent(Person1,Someperson),
    ancestor(Someperson,Person2).
