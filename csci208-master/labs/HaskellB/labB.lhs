Bryce DiRisio
labB.lhs

>fancyDiv::Int->Int->(Int,Int)
>fancyDiv n m = if (m == 0)
>               then error "dont divide by 0\n"
>               else (quotient,remainder)
>               where
>                   quotient = n `quot` m
>                   remainder = n `rem` m

Problem 6

>roots:: Int->Int->Int->(Float,Float)
>roots a b c = if (fromIntegral(b)**2 - 4*fromIntegral(a)*fromIntegral(c) < 0)
>               then error "error - roots are not real"
>               else (real1, real2)
>
>               where
>                   real1 = (-fromIntegral(b) + sqrt(fromIntegral(b)**2 - 4*fromIntegral(a)*fromIntegral(c)))/(2*fromIntegral(a))
>                   real2 = (-fromIntegral(b) - sqrt(fromIntegral(b)**2 - 4*fromIntegral(a)*fromIntegral(c)))/(2*fromIntegral(a))
>
>allroots:: Int->Int->Int->((Float,Float),(Float,Float))
>
>allroots a b c = if(fromIntegral(b)**2 - 4*fromIntegral(a)*fromIntegral(c)) < 0
>                   then ((root, img),(root,(-img)))
>                 else ((real1, 0.0),(real2, (-0.0)))
>             
>                   where
>                       real1 = (-fromIntegral(b) + sqrt(fromIntegral(b)**2 - 4*fromIntegral(a)*fromIntegral(c)))/(2*fromIntegral(a))
>                       real2 = (-fromIntegral(b) - sqrt(fromIntegral(b)**2 - 4*fromIntegral(a)*fromIntegral(c)))/(2*fromIntegral(a))
>                       img = sqrt(abs(fromIntegral(b)**2 - 4*fromIntegral(a)*fromIntegral(c)))/2
>                       root = -fromIntegral(b)/(2*fromIntegral(a))
>
>
>
>
>

Problem 7

When using head[1..n] I observed that the time it takes to evaluate the expression actually stays the same, as the vause of n increases. 

When using tail[1..n], I observed that the larger n is, the longer it took to evaulate this expression. This is because when you evaulate 'head', it only takes the first list element, 1. But, when you evaulate 'tail', you have a list of length (n-1) you have to evaluate and return.

Problem 8

>firstLast::[a]->[a]
>
>firstLast [] = error "inputted list is empty"
>firstLast l = [x|x<-(tail(init l))]
>
>strip::Int->[Int]->[Int]
>
>strip n [] = error "inputted list is empty"
>strip n l = [x | x<-reverse(drop n (reverse(drop n l)))]
>
>mrg::[Int]->[Int]->[Int]
>
>mrg a [] = a
>mrg [] b = b
>mrg (a:as) (b:bs) = if(b<a)
>                        then b:mrg (a:as) bs
>                    else a:mrg as (b:bs)
>

Problem 9

Pattern             Argument     Succeeds?            Bindings

1                      1             y                  n/a
2                      1             y                  n/a
x                      1             y                 x = 1
x:y                  [1,2]           y              x = 1, y = [2]
x:y                 [[1,2]]          y            x = [1,2], y = []    
x:y                "Bucknell"        y         x = "B", y = "ucknell"
x:y                ["Bucknell']      y         x = "Bucknell", y = []
x:y:z              [1,2,3,4,5]       n
x                      []            y                  x = []
[1,x]                 [2,2]          n  
[]                    [2,2]          n
(x,y)               [1,2,3,4]        n
((x:y),(z:w))   ([1],"Bucknell")     y    x = 1, y = [], z = "B", w = "ucknell"

Problem 10

>addrat::(Int,Int)-> (Int,Int)->(Int,Int)
>
>addrat (_,0) (_,_) = error "Can't divide by zero!"
>addrat (_,_) (_,0) = error "Can't divide by zero!"
>addrat (a,b) (x,y) = if (y==b)
>                        then ((a+x),y)
>                     else ((a*y)+(b*x), (b*y))
>
>

Problem 11

> 
>sorted::[Int]->Bool
>
>sorted l = if ((length l) == 1)
>               then True
>           else if ((l !! 0) < (l !! 1))
>               then sorted (tail l)
>           else False
>
>helperFunc:: [Int] -> Int -> Int
>
>helperFunc l cur = if ((length l) == 0)
>                   then cur
>                else helperFunc (tail l) (cur + (head l))
>
>mySum :: [Int] -> Int
>
>mySum l = helperFunc l 0
