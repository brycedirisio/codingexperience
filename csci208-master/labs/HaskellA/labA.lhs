This file contains the definition for the factorial function.

>fact :: Int -> Int
>
>fact 0 = 1
>fact n = n * fact (n-1)
>
>choose1 :: Int
>
>choose1 = fact(10) `div` (fact(5) * fact(10-5))
>
>--This should return the int 252, which is the evaulated expression for how many combinations of 5 you
>--Can take from a set of 10 items.

>--When fact 0 is commented out, we see a stack overflow error. This happens because our fact function
>--has no exit condition, so it will keep recursively calling itself over and over again until the number gets so
>--large it cannot be held by the system. To correct it, simply include the exit condition here (that is, 'fact 0 = 1').
>
>--The parenthesis on "fact(n-1)" are not necessary, since the "fact" term is technically the only
>--thing being evaluated in the parenthesis. However, the parenthesis around "n-1" are necessary for
>--the program, since otherwise the program recursively calls itself again with no change (fact n will
>--call fact n will call fact n will call ...).

>choose::Int->Int->Int 
>
>choose n 0 = 1
>choose n k = div (fact (n)) (fact(k) * fact(n-k))

>--When you do fact 50, we see a negative number returned to us (-3258495067890909184), and when we do choose 50 5, 
>--we see '0' returned to us.

>choose2::Int->Int->Int
>
>choose2 n 0 = 1
>choose2 0 k = 0
>choose2 n k = (choose2 (n-1) (k-1)) + (choose2 (n-1) k)

>fib :: Int->Int
>
>fib 0 = 0
>fib 1 = 1
>fib n = fib(n-1) + fib(n-2)

>--TEST: fib 0 = 0, fib 1 = 1, fib 2 = 1, fib 3 = 2, fib 4 = 3, fib 5 = 5, fib 6 = 8, fib 7 = 13, ...

>fibse1::Int->Int
>
>fibse1 n = if n == 0
>        then 0
>        else if n == 1
>                then 1
>                else fibse1(n-2) + fibse1(n-1)

>fibcase::Int->Int
>
>fibcase n = case n of
>           0->0
>           1->1
>           n->fib(n-1) + fib(n-2)

>grade::Int->Char
>
>grade n
>   | (100 >= n) && (n >= 90)   = 'A'
>   | (90  > n) && (n >= 80)    = 'B'
>   | (79  > n) && (n >= 70)    = 'C'
>   | (69  > n) && (n >= 60)    = 'D'
>   | (59  > n) && (n >= 0)     = 'F'
>   | otherwise                 = 'E'

>abs2::Int->Int
>
>abs2 n
>   | (n<0) = -n
>   | otherwise = n

>hyp::Int->Int->Float
>
>hyp 0 b = 0
>hyp a 0 = 0
>hyp a b
>    | a<0 = 0
>    | b<0 = 0
>    | otherwise = sqrt(fromIntegral(a) * fromIntegral(a) + fromIntegral(b) * fromIntegral(b))
