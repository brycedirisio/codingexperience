>import Data.Char
>import Data.List

12.1

>absolute::[Int]->[Int]
>
>absolute list = [abs(x) | x<-list]

12.2

>string::[String]->[Int]
>
>string list = [length (x)| x<-list]

13.1

>help1::Int->Int->Int
>help1 0 y = y
>help1 x y = help1 (x-1) (y*10)
>
>help2::[Char]->Int->Int
>
>help2 [] cur = cur
>help2 list cur = help2 (tail list) (cur + (digitToInt (head list) * (help1 (length list - 1) 1)))
>
>convert2Int::[Char]->Int
>
>convert2Int list = help2 list 0

13.2

>hexHelp::[Char]->Int->Int
>
>hexHelp [] cur = cur
>hexHelp list cur = hexHelp (tail list) (cur + (digitToInt (head list) * (help1 (length list - 1) 1)))

>convertHex2Int::[Char]->Int
>convertHex2Int list = hexHelp list 0

14

>help3 x = x^2

>stdDev::[Float]->Float
>
>stdDev list = sqrt((foldl (+) 0 (map help3 list)/(fromIntegral(length list))) - (help3((foldl (+) 0 list)/(fromIntegral(length list)))))

16

>data Cplx = Complex Float Float
>
>instance Show Cplx where
>       show (Complex a b) = (show a) ++ " + " ++ (show b) ++ "i"
>
>
>roots::Int->Int->Int->(Cplx, Cplx)
>
>roots a b c = ((Complex real imag), (Complex real (-1*imag)))
>       where
>               real = -1*fromIntegral(b)/(2*fromIntegral(a))
>               imag = (fromIntegral(b)^2)-(4*fromIntegral(a)*fromIntegral(c))
