//Bryce DiRisio
//letter.cpp

#include <iostream>

using namespace std;

int main(){
    string first_name;
    string friend_name;
    char friend_diet = 'x';
    string meal;
    int age;
    string bday;
    cout << "Please enter your first name: " << endl;   //PROBLEM 1
    cin >> first_name;
    cout << "\nHello " << first_name << "!\n" << endl;
    cout << "Please enter the name of a person you want to invite for dinner." << endl;     //PROBLEM 2
    cin >> friend_name;     //PROBLEM 3
    cout << "\nDoes this friend have any dietary restrictions? Type 'a' for none, 'v' for vegetarian, or 'g' for vegan" << endl;
    cin >> friend_diet;
    cout << "\nHow old is " << friend_name <<  "?" << endl;   //PROBLEM 5
    cin >> age;
    ++age;
    if (age == 16){
        bday = "I heard that it's almost your sweet 16!";
    }
    else if (age == 18){
        bday = "I heard you're turning 18 soon!";
    }
    else if (age == 21){
        bday = "I heard you're turning 21 soon!";
    }
    else if (age == 50){
        bday = "I heard it's almost your 50th birthday!";
    }
    else if (age == 100){
        bday = "I heard it's almost your 100th birthday!";
    }
    if (friend_diet == 'a'){       //PROBLEM 4
        meal = "Roasted Chicken";
    }
    else if (friend_diet == 'v'){
        meal = "French Toast with Honey";
    }
    else if (friend_diet == 'g'){
        meal = "Avocado Toast";
    }
    cout << "\nHello " << friend_name << "! " << bday << " Would you like some " << meal << "?\n" << endl;
    return 0;
}
