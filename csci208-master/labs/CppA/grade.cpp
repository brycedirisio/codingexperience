//Bryce DiRisio
//CSCI 208 - CppA
//grade.cpp

#include <iostream>
#include <vector>

using namespace std;

int main(){
    vector<int> vec;
    cout << "Type in several integers between 0 and 10 :" << endl;
    int i = 0;
    int input = 0;
    while(cin >> input){
        vec.push_back(input);
    }
    int gradeAvg = 0;
    for (int i = 0; i < vec.size(); i++){
        gradeAvg = gradeAvg + vec[i];
    }
    float finalGradeAvg = gradeAvg / vec.size();
    char grade;
    if(finalGradeAvg > 90){
        grade = 'A';
    }
    else if(finalGradeAvg > 80){
        grade = 'B';
    }
    else if (finalGradeAvg > 70){
        grade = 'C';
    }
    else if (finalGradeAvg > 65){
        grade = 'D';
    }
    else{
        grade = 'F';
    }
    cout << "Your final letter grade is: " << grade << endl;

    return 0;
}
