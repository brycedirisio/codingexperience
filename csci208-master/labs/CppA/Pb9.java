//Bryce DiRisio
//pb9.java

public class Pb9{
    int value = 0;
    void print(){
        System.out.println(value);
    }
    void change(int x){
        value = x;
    }


public static void main(String[] args) {
    Pb9 obj1 = new Pb9();
    Pb9 obj2 = obj1;
    obj1.print();
    obj2.print();
    obj1.change(42); // change a member data to 42
    obj1.print();
    obj2.print();
    }
}
