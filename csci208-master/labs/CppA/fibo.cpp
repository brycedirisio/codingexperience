//Bryce DiRisio
//fibo.cpp

#include <iostream>

using namespace std;

int fibo(int n){
    if (n == 0){
        return 0;
    }
    else if (n == 1){
        return 1;
    }
    else{
        return fibo(n-1) + fibo(n-2);
    }
}

int main(){
    for(int i = 0; fibo(i) > -1 ;++i){
        cout << "fibo(" << i << ") = " << fibo(i) << endl;
    }
    cout << "SUSPICIOUSLY WRONG!" << endl;
    return 0;
}
