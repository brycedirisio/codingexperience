/*Bryce DiRisio
 * pb8_b.cpp
 */
#include <iostream>
#include "pb8_a.h"

using namespace std;

void reinit(){
  global_var = 0;
}

void print(){
  cout << "global_var = " << global_var << endl;
}
