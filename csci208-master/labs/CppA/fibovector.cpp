//Bryce DiRisio
//CSCI208 - CppA
//fibovector.cpp

#include <iostream>
#include <vector>

using namespace std;

int fibo(vector<int> &table, int n){
    if(n < table.size()){
        return table[n];
    }
    else{
            int value = fibo(table, n-1) + fibo(table, n-2);
            table.push_back(value);
            return value;
        }
}
int main(){
    vector<int> memo;
    memo.push_back(0);
    memo.push_back(1);
    for(int i = 0; fibo(memo, i) > -1 ;++i){
        cout << "fibo(" << i << ") = " << fibo(memo, i) << endl;
    }
    cout << "SUSPICIOUSLY WRONG!" << endl;
    return 0;
}

