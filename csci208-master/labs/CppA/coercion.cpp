//Bryce DiRisio
//coercion.cpp

#include <iostream>

using namespace std;

int main(){
    cout << "size of 'char' type: " << sizeof(char) << endl;
    cout << "size of 'bool' type: " << sizeof(bool) << endl;
    cout << "size of 'short' type: " << sizeof(short) << endl;
    cout << "size of 'int' type: " << sizeof(int) << endl;
    cout << "size of 'float' type: " << sizeof(float) << endl;
    cout << "size of 'long' type: " << sizeof(long) << endl;
    cout << "size of 'double' type: " << sizeof(double) << endl;
    cout << "size of 'long long' type: " <<sizeof(long long) << endl;
    cout << "size of 'long double' type: " <<sizeof(long double) << endl;
    cout << endl;
    cout << "increasing order of types: char, bool, short, int, float, long, double, long long, long double\n" <<endl;
    
    //safe coercion
    cout << "Here is a safe implicit coercion:" << endl;
    double d = 3.14;
    float f = d;
    cout << "Our double, d, is: " << d << endl;
    cout << "Our double -> float, f, is: " << f << "\n" << endl;

    //unsafe coercion
    cout << "Here are two unsafe implicit coercions: " << endl;
    double d2 = 3.14;
    int i = d2;
    cout << "Our double, d, is " << d2 << endl;
    cout << "Our double -> integer, i, is " << i << "\n" << endl;

    bool b = 1;
    char c = b;
    cout << "Our boolean, b, is " << b << endl;
    cout << "Our boolean -> char, c, is " << c << "\n" << endl;

    cout << "In terms of weakly vs strongly typed languages, I\n would argue that c++ is a strongly typed language. This is mostly\n because you are required to declare your variable types\n if you wish to work with them (i.e. saying 'int x;'\n rather than just 'x=3. The latter would give you an error if ran.\n Because of this error, we know that checking the variable\n types is done during compilation rather than runtime, so it is\n strongly typed.\n" << endl;
    return 0;
}
