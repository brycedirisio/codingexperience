//Bryce DiRisio
//refpointer.cpp

#include <iostream>
using namespace std;

int main(){
    //PROBLEM 1
    //int &r; 
    //No, you cannot declare a reference without initializing it (it will error if uncommented).

    //PROBLEM 2
    int *p1; 
    //Yes, it is possible to delcare a pointer without initializing it.

    //PROBLEM 3
    int i = 1;
    int j = 2;
    int &r = i;
    r = j;
    //In a sense, you could change what a reference refers to in the above way. The issue with this, however is that you would also augmate the original variable being referenced.
    
    //PROBLEM 4
    *p1 = i;
    *p1 = j;
    //Here, we see the pointer from problem 2 get initialized to point to
    //the variable i, then reassigned to point to the variable j. These
    //variables have to be stored in different memory addresses, so yes
    //it is possible.
    
    
    //PROBLEM 5
    /*The code provided in the problem can be found in the file 'pb3_pt5.cpp'.
     *When compiled and ran, the program outputted '10 10 10'.
     *
     *The reason why this happened is because when the
     *p += r; line happens, we see that r is a reference
     *to the pointer p, which points to the reference i,
     *which is 5. Thus, the value in i goes up to 10, the
     *pointer *p points to i so it holds 10 too, and 
     *reference &r goes to *p, so it will also hold 10.
     *Thus, the print statement is only going to print '10'.
     *
     */
        
    return 0;
}
