/* Bryce DiRisio
 * pb9.cpp
 *
 * This code will compile and run
 */

#include <iostream>

using namespace std;

class Pb9{
    private:
    public:
        int value = 0;
    int print(){
        cout << value << endl;
    }

    int change(int j){
        value = j;
    }
};

int main(){
    Pb9 obj1 = Pb9(); // notice we did NOT use new here
    //Pb9 obj2 = obj1; 
    Pb9 &obj2 = obj1;//By changing this line so that obj2 is a
                     // reference to obj1 rather than a separate
                     // instance of obj1, we can mimic the output
                     // that the java program will give us.
    obj1.print();
    obj2.print();
    obj1.change(42); // change a member data to 42
    obj1.print();
    obj2.print();
}


