def recursiveNumDigits(x):
    if x < 10:
        return 1
    else:
        return 1 + recursiveNumDigits(x/10)

#tail recursive solution for problem 1
def tailRecursiveNumDigits(x, runningTally=0):
    if x < 10:
        return 1 + runningTally
    else:
        return tailRecursiveNumDigits(x/10, runningTally + 1)

def iterativeFunction(n):
    summation = 0
    while (n > 1):
        summation += n
        n = n-1
    summation += 5
    return summation
