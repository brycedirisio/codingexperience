public class Door {
    int numHinges;
    boolean lockable;
    Door(){}
    Door(Door doors){
        this.numHinges = doors.numHinges;
        this.lockable = doors.lockable;
    }
    public String toString() {
	if (lockable)
	    return "Lockable with " + numHinges + " hinges";
	else
	    return "Plain with " + numHinges + " hinges";	
    }
}
