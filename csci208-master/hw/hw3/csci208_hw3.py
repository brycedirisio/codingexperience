#Bryce DiRisio
#CSCI 208
#Homework 3

#The 'main' function is declaring a string and an integer. It then converts
#the string to an integer and adds the two together. It also converts the
#integer to a string and concatenates the two together to produce the
#integer 14 and string "59".

def main():
    i = 5
    string = "9"
    c = str(i) + str(string)
    d = int(i) + int(string)
    print("here is the string representation: ")
    print(c)
    print("here is the integer representation: ")
    print(d)

main()
