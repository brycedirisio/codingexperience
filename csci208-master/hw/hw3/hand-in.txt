Bryce DiRisio
CSCI208 - hw3
hand-in.txt

Problem 1) Java is a strongly typed language

Problem 2) Done.

Problem 3) Done.

Problem 4) Done.

Problem 5) Here, we see that mathematical operations only work when the types
of variables are the same. Thus, FlyingToaster is a 'strongly typed' language
(NO AUTOMATIC COERCIONS).

Problem 6) What we see here is that in Foozle, it allows the variable, 'x', to change from a boolean to the class 'Apple'. In Banana, this coercion is not allowed. Thus, we see that These languages can be either 'Dynamic', or 'Static'.

The difference between these two options is that a dynamic language is concerned
with types while the program is running, whereas a static language is concerned with
types only when the program gets compiled. Thus, dynamic languages would usually
be good at coercing between types over static languages.

In problem 6, Foozle is dynamic, while Banana is static.
