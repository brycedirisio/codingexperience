public class problem4{

class Animal{
    void p(){
        System.out.println("a");
    }
void q(){
    System.out.println("b");
}
void use(){
    p();
    q();
}
}

class Dog extends Animal{
    void p(){
        System.out.println("b");
    }
    void q(){
        System.out.println("a");
    }
    void use(){
        p();
        q();
    }
}

public static void main(String[] args){
    Animal a = new Animal();
    a.use();
    Dog a2 = new Dog();
    a2.use();
}
}
/*This code, when compiled, will.
 * Java by default uses dynamic binding,
 * as shown above
*/
