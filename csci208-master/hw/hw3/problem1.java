public class problem2{

public static void main(String[] args){
    int a = 3;
    float b = 3.14159;
    float c = a;
    System.out.print(c);
    int d = b;
    System.out.print(d);
}

}
/* This problem will give an error, since it
 * cannot coerce the float, b, into the int, d.
 * This is due to the data loss that occurs when
 * a float tries to get shortened into a int.
*/
