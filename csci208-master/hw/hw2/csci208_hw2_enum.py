import enum

class Animal(enum.Enum):
    DOG = 1
    CAT = 2
    PARROT = 3

print(Animal.DOG)
print(Animal.CAT)
print(Animal.PARROT)
print(repr(Animal.DOG))
print(type(Animal.DOG.name))
