Midterm project: StockerBot

The is is the midterm project of Bill Chen, Bryce DiRisio, and Nicholas Sadd. Our goal for this project was to create a pattern recognition software that took in the stock values of many different stocks and use them to predict whether one particular stock's value will increase or decrease within the next day/week/month.

As of right now we are using the Keras neural network API to try and prove our concept. In the future we plan on implementing our own neural network from scratch using TensorFlow. 

What we are using:
Keras,
Kaggle ("Huge Stock Market Dataset: Historical daily prices and volumes of all U.S. stocks and ETFs", Boris Marjanovic, 2017),

TO RUN THE PROGRAM:


    1) Open up "stockerBot.py" using your favorite text editor
    
    2) scroll down to line 32 in the program
    
    3) change the two parameters according to the commented sections.
    
        3a) the first variable (DATA_FILE) outlines which txt file you want to analyze. It is automatically set to analyze microsoft stocks, but additional data sets are provided if you wish to choose it
        
        3b) The second variable (INPUT_PATH) outlines the path towards the folder holding all of the txt and python files included in the project. Simply substitute this path with your own path to wherever you are storing the project.
        
    4) Save and close the program
    
    5) Make sure that a file called 'outputs' does not currently exist in the folder. If it does, delete the 'outputs' file.
    
    6) Run the program (go to command line and type in 'python stockerBot.py')
    