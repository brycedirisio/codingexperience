# csci349_FinalProject

This repository contains the final project completed by Bryce DiRisio for Bucknell's CSCI349 - Intro to Data Mining class.

Dataset is from the following website: https://www.kaggle.com/cites/cites-wildlife-trade-database

This dataset contains information regarding wildlife trade for many different species across many different countries. The
purpose of the project is to try and find frequent patterns that may emerge from the global wildlife trade market, especially
looking into international country trades. By doing so, I am hoping to come up with common patterns persistent in this market
and to look into what products/animal goods are most popular in the more active communities. 

Additionally, I am using this project in order to find a way to classify transaction items by looking at the quantities of items
being exchanged, and looking into predictive measures using this category. Predictive models like this could be especially useful
in tracking down and evaluating what items are being sold, even in the abscence of information. 