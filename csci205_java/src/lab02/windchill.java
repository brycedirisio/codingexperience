package lab02;

//import java.util.Scanner;

public class windchill {
	private double Vel;
	private double Temp;
	
	public double get_Vel() {
		return Vel;
	}
	
	public double get_Temp() {
		return Temp;
	}
	
	public void set_Vel(double temp) {
		Vel = temp;
	}
	
	public void set_Temp(double temp) {
		Temp = temp;
	}
	
	public Wind_Chill(double tempA, double tempB) {
		Vel = tempA;
		Temp = tempB;
	}
	
	public double original_method() {
		return (10*Math.sqrt(Vel)-Vel+10.5) * (33-Temp);
	}
	
	public double canada_model() {
		Vel = 0.277778*Vel; //conversion from k/hr to m/sec
		return 13.12 + (0.6215*Temp) - (11.37*Math.pow(Vel, 0.16)) + (0.3965*Temp*Math.pow(Vel,  0.16));
	}
	
	public double us_model() {
		Temp = (Temp*1.8) + 32;
		Vel = Vel*(double) (25/11);
		return 35.74 + (0.6215*Temp) - (35.75*Math.pow(Vel,  0.16)) + (0.4275*Temp*Math.pow(Vel, 0.16));
	}
	
	public static void main(String[] args) {
		System.out.println("Enter a wind speed in meters/second:  ");
		Scanner sc = new Scanner(System.in);
		double Vel = sc.nextDouble(); 
		
		System.out.println("Enter the air temperature in degrees celsius: ");
		double Temp = sc.nextDouble();
		
		sc.close();
		
		Wind_Chill IO = new Wind_Chill(Vel, Temp);
		
		System.out.println("The wind chill using the original method is " + IO.original_method());
		System.out.println("The wind chill using the Canadian method is " + IO.canada_model());
		System.out.println("The wind chill using the US method is " + IO.us_model());
		
	}
}



