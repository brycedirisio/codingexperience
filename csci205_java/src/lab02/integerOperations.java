package lab02;

import java.util.Scanner;


public class integerOperations {
	


		private int num1;
		private int num2;
		
		public integerOperations(){
		}
		
		public void setInt1(int a) {
			num1 =  a;
		}
		
		public void setInt2(int b) {
			num2 =  b;
		}
		
		public void setInt(int a, int b) {
			num1 = a;
			num2 = b;	
			
		}
		
		public int getInt1() {
			return num1;
		}
		
		public int getInt2() {
			return num2;
		}
		 
		public int add() {
			return num1 + num2;
		}
		
		public int sub() {
			return num1 - num2;
		}
		
		public int mult() {
			return num1 * num2;
		}
			
		public int div() {
			return num1 / num2;
		}

		
		
		public static void main(String[] args) {
			integerOperations int_op = new integerOperations();
			System.out.println("Enter integer a ");
			Scanner sc = new Scanner(System.in);
			int a = sc.nextInt(); 
			System.out.println("Enter integer b ");
			int b = sc.nextInt();
			sc.close();
			int_op.setInt(a,b);
			
			System.out.println(int_op.getInt1() + " + " + int_op.getInt2() + " = " + int_op.add());
	        System.out.println(int_op.getInt1() + " * " + int_op.getInt2() + " = " + int_op.mult());
	        System.out.println(int_op.getInt1() + " / " + int_op.getInt2() + " = " + int_op.div());
	        System.out.println(int_op.getInt1() + " - " + int_op.getInt2() + " = " + int_op.sub());
	        
		}

}



