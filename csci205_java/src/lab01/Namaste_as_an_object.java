
public class Namaste_as_an_object {
private String str;
	
	// constructor
	
	public Namaste_as_an_object(String string) {
		str=string;
	}

	// first step is to have get and set methods	

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
	void instanceNamaste() {
		
	       System.out.println(getStr());
	  }

	public String[] getArray(String str){
		String[] ary = str.split("");
		return ary;
		
	}
	
	public void print_array(String[] s){
		for (int i = 0; i < s.length; i++)
		    System.out.println(s[i]);
	}
	
	public static void main(String[] args) {
		Namaste_as_an_object h= new Namaste_as_an_object("Hello_World");
		h.instanceNamaste();
		//String[]s=h.getArray("Hello_World_2");
		//h.print_array(h.getArray("Hello_World_2"));
		//System.out.println(s[0]);
		h.setStr("Hello_World_2");
		h.instanceNamaste();

	}

}

