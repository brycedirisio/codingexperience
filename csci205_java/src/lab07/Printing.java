package lab07;


public class Printing extends Matrix{
	public Printing (double[][] data) {
		super(data);
	}
	public void print() {
        for (int i = 0; i < super.getrow(); i++) {
            for (int j = 0; j < super.getcol(); j++) 
                System.out.printf("%9.4f ", data[i][j]);
            System.out.println();
        }
    }
	public static void main(String[] args) {
		double[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Printing p = new Printing(m);
		p.print();
	}
}
