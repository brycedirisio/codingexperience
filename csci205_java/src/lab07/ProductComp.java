package lab07;


public class ProductComp {
private Matrix mat;
	public ProductComp(Matrix mat) {
		this.mat = mat;
	}
	public Matrix product(Matrix B) {
        if (mat.getcol() != B.getrow()) throw new RuntimeException("Illegal matrix dimensions.");
        Matrix C = new Matrix(mat.getrow(), B.getcol());
        for (int i = 0; i < C.getrow(); i++)
            for (int j = 0; j < C.getcol(); j++)
                for (int k = 0; k < mat.getcol(); k++)
                    C.data[i][j] += (mat.data[i][k] * B.data[k][j]);
        return C;
    }
	public static void main(String[] args) {
		double[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Matrix bree = new Matrix(m);
		ProductComp p = new ProductComp(bree);
		double[][] t = { { 2, 2, 2 }, { 2, 2, 2 }, { 2, 2, 2} };
		Matrix b = new Matrix(t);
		p.product(b);
		System.out.println(p.product(p.product(b)));
	}
}
