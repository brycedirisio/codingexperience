package lab07;


public class SubtractionComp {
private Matrix mat;
	public SubtractionComp(Matrix mat) {
		this.mat = mat;
	}
	public Matrix subtraction(Matrix B) {
        if (B.getrow() != mat.getrow() || B.getcol() != mat.getcol()) throw new RuntimeException("Illegal matrix dimensions.");
        Matrix C = new Matrix(mat.getrow(), mat.getcol());
        for (int i = 0; i < mat.getrow(); i++)
            for (int j = 0; j < mat.getcol(); j++)
                C.data[i][j] = mat.data[i][j] - B.data[i][j];
        return C;
    }
	public static void main(String[] args) {
		double[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Matrix bree = new Matrix(m);
		SubtractionComp s = new SubtractionComp(bree);
		double[][] t = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1} };
		Matrix b = new Matrix(t);
		s.subtraction(b);
		System.out.println(s.subtraction(b));
	}
}

