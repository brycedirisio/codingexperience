package lab07;


public class WindChillUS extends Windchill {
	//constructor
	
	public WindChillUS(double temp, double vel){
		super(temp,vel);
	}
	
	
	
		
	
	public double compute(){
		return 35.74 + 0.6215*super.gettemp() + (0.4275*super.gettemp() - 35.75) * Math.pow(super.getvel(), 0.16);
		
	}

	public static void runthis(double t, double v){
		WindChillUS g1=new WindChillUS(t,v);
    	System.out.println("Temperature = " + g1.gettemp());
    	System.out.println("Wind speed  = " + g1.getvel());
    	System.out.println("Wind chill  = " + g1.compute());
		
	}
	
	 public static void main(String[] args) {
	    	runthis(5,50);

}
}