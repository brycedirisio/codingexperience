package lab07;


public class Subtraction extends Matrix{
	public Subtraction (int M, int N) {
		super(M, N);
	}
	public Subtraction (double[][] data) {
		super(data);
	}
	public Matrix subtraction(Matrix B) {
        Matrix A = this;
        if (B.getrow() != A.getrow() || B.getcol() != A.getcol()) throw new RuntimeException("Illegal matrix dimensions.");
        Matrix C = new Matrix(super.getrow(), super.getcol());
        for (int i = 0; i < super.getrow(); i++)
            for (int j = 0; j < super.getcol(); j++)
                C.data[i][j] = A.data[i][j] - B.data[i][j];
        return C;
    }
	public static void main(String[] args) {
		double[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Subtraction a = new Subtraction(m);
		double[][] t = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1} };
		Matrix b = new Matrix(t);
		a.subtraction(b);
		System.out.println(a.subtraction(b));
	}
}
