package lab07;

public class WindChillUSComp {
	private Windchill wc;
	
	// constructor
	public WindChillUSComp(Windchill wc){
		this.wc=wc;
		
	}

public  double compute(){
	return 35.74 + 0.6215*wc.gettemp() + (0.4275*wc.gettemp() - 35.75) * Math.pow(wc.getvel(), 0.16);
	
}



public static   void runthis(double t, double v){
	 Windchill h= new Windchill(t,v);
	 WindChillUSComp h1=new WindChillUSComp(h);
	System.out.println("Temperature = " + h.gettemp());
	System.out.println("Wind speed  = " + h.getvel());
	System.out.println("Wind chill  = " + h1.compute());
	
}

 public static void main(String[] args) {

    	runthis(5,50);

}

}
