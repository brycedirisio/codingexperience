package lab07;

public class WindChillWorldComp {
	private Windchill wc;
	
	// constructor
	public WindChillWorldComp(Windchill wc){
		this.wc=wc;
		
	}
	public double vel(){
		return 0.44704*wc.gettemp();
	}
	public double ftemp(){
		return 5*(wc.getvel()-32)/9;
	}
	
	public double compute(){
		return (10*Math.pow(vel(),0.5)-vel()+10.5)*(33-ftemp());
		
	}



public static   void runthis(double t, double v){
	 Windchill h= new Windchill(t,v);
	 WindChillCanadaComp h1=new WindChillCanadaComp(h);
	System.out.println("Temperature = " + h.gettemp());
	System.out.println("Wind speed  = " + h.getvel());
	System.out.println("Wind chill  = " + h1.compute());
	
}

 public static void main(String[] args) {

    	runthis(5,50);

}

}
