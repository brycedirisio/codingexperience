package lab07;


public class TransposeComp {
private Matrix mat;
	public TransposeComp(Matrix mat) {
		this.mat = mat;
	}
	public Matrix transpose() {
        for (int i = 0; i < mat.getrow(); i++)
            for (int j = 0; j < mat.getcol(); j++)
                mat.data[j][i] = mat.data[i][j];
        return mat;
	}
	public static void main(String[] args) {
		double[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Matrix bree = new Matrix(m);
		TransposeComp r = new TransposeComp(bree);
		r.transpose();
		System.out.println(r.transpose());
	}
}
