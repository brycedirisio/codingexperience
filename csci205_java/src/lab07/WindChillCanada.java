package lab07;


public class WindChillCanada extends Windchill{
	public WindChillCanada(double temp, double vel){
		super(temp,vel);
	}
	public double vel(){
		return 1.60934*super.getvel();
	}
	public double ftemp(){
		return 5*(super.gettemp()-32)/9;
	}
	
	public double compute(){
		return 13.12+0.6215*ftemp()-11.37*Math.pow(vel(), 0.16)+0.3965*ftemp()*Math.pow(vel(), 0.16);
		
	}
	
	public static void runthis(double t, double v){
		WindChillCanada g1=new WindChillCanada(t,v);
    	System.out.println("Temperature = " + g1.gettemp());
    	System.out.println("Temperature in Deg C= " + g1.ftemp());
    	System.out.println("Wind speed  = " + g1.getvel());
    	System.out.println("Wind speed in Km/Hr = " + g1.vel());
    	System.out.println("Wind chill  = " + g1.compute());
		
	}
	
	 public static void main(String[] args) {
	    	runthis(5,50);

}
	
	
}