package lab07;

public class WindChillWorld extends Windchill {
	//constructor
	
	public WindChillWorld(double temp, double vel){
		super(temp,vel);
	}
	
	
	public double vel(){
		return 0.44704*super.gettemp();
	}
	public double ftemp(){
		return 5*(super.getvel()-32)/9;
	}
	
	public double compute(){
		return (10*Math.pow(vel(),0.5)-vel()+10.5)*(33-ftemp());
		
	}
	
	public static void runthis(double t, double v){
		WindChillCanada g1=new WindChillCanada(t,v);
    	System.out.println("Temperature = " + g1.gettemp());
    	System.out.println("Temperature in Deg C= " + g1.ftemp());
    	System.out.println("Wind speed  = " + g1.getvel());
    	System.out.println("Wind speed in Km/Hr = " + g1.vel());
    	System.out.println("Wind chill  = " + g1.compute());
		
	}
	
	 public static void main(String[] args) {
	    	runthis(5,50);

}
}