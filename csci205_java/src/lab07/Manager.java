package lab07;

import java.util.Date;

public class Manager extends Employee {

	private String deptName;
	public Manager(int empID, String firstName, String lastName, int ssNum, Date hireDate, double salary, String deptName) {
		super(empID, firstName, lastName, ssNum, hireDate, salary);
		// TODO Auto-generated constructor stub
		this.deptName = deptName;
	}
	public String toString() {
		return super.toString() + ",Manager," + this.getDeptName();
	}
	private String getDeptName() {
		// TODO Auto-generated method stub
		return deptName;
	}

}
