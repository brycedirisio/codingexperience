package lab07;


public class Transpose extends Matrix{
	public Transpose (double[][] data) {
		super(data);
	}
	public Matrix transpose() {
        Matrix A = new Matrix(super.getrow(), super.getcol());
        for (int i = 0; i < super.getrow(); i++)
            for (int j = 0; j < super.getcol(); j++)
                A.data[j][i] = this.data[i][j];
        return A;
    }
	public static void main(String[] args) {
		double[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Transpose r = new Transpose(m);
		r.transpose();
		System.out.println(r.transpose());
	}
}
