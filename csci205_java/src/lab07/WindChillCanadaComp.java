package lab07;

public class WindChillCanadaComp {
	private Windchill wc;
	
	// constructor
	public WindChillCanadaComp(Windchill wc){
		this.wc=wc;
		
	}
	public double vel(){
		return 1.60934*wc.getvel();
	}
	public double ftemp(){
		return 5*(wc.gettemp()-32)/9;
	}
	
	public double compute(){
		return 13.12+0.6215*ftemp()-11.37*Math.pow(vel(), 0.16)+0.3965*ftemp()*Math.pow(vel(), 0.16);
		
	}



public static   void runthis(double t, double v){
	 Windchill h= new Windchill(t,v);
	 WindChillCanadaComp h1=new WindChillCanadaComp(h);
	System.out.println("Temperature = " + h.gettemp());
	System.out.println("Wind speed  = " + h.getvel());
	System.out.println("Wind chill  = " + h1.compute());
	
}

 public static void main(String[] args) {

    	runthis(5,50);

}

}
