package lab07;


public class Product extends Matrix{
	public Product(int M, int N) {
		super(M, N);
	}
	public Product (double[][] data) {
		super(data);
	}
	public void product(Matrix B) {
        Matrix A = this;
        if (A.getcol() != B.getrow()) throw new RuntimeException("Illegal matrix dimensions.");
        Matrix C = new Matrix(A.getrow(), B.getcol());
        for (int i = 0; i < C.getrow(); i++) {
            for (int j = 0; j < C.getcol(); j++) {
                for (int k = 0; k < A.getcol(); k++) {
                    C.data[i][j] += (A.data[i][k] * B.data[k][j]);
                }
                System.out.print(C.data[i][j] + " ");
            }
            System.out.println();
        }
        //return C;
    }
	public static void main(String[] args) {
		double[][] m = { { 0.1, 0, 0 }, { 0.3, 0, 0 }, { 0.9, 0, 0} };
		Product p = new Product(m);
		double[][] t = { {0.04481084*(.634*(1-0.634)) , 0.01639332*(0.567*(1-0.567)), 0.03974643*(0.74*(1-0.74)) }, { 0, 0, 0 }, { 0, 0, 0} };
		Matrix b = new Matrix(t);
		p.product(b);
		//System.out.println(p.product(p.product(b)));
	}
}
