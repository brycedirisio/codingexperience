package lab07;


public class Matrix {
private final int M;             // number of rows
private final int N;             // number of columns
protected final double[][] data;
    
    public Matrix(int row, int col) {
        M = row;
        N = col;
        data = new double[M][N];
        
    }
	public Matrix(double[][] data) {
	    M = data.length;
	    N = data[0].length;
	    this.data = new double[M][N];
	    for (int i = 0; i < M; i++)
	        for (int j = 0; j < N; j++)
	                this.data[i][j] = data[i][j];
	}
    public int getrow() {
    		return M;
    }
    public int getcol() {
    		return N;
    }
}
