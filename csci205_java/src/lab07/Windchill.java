package lab07;

public class Windchill {
	protected double temp;
	protected double vel;
	// constructor
	public Windchill(double temp, double vel){
		this.temp=temp;
		this.vel=vel;		
	}
	public double getvel(){
		return this.vel;		
	}
	public double gettemp(){
		return this.temp;		
	}
	
  
}