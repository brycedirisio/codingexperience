package lab07;


public class PrintingComp {
private Matrix mat;
	public PrintingComp(Matrix mat) {
		this.mat = mat;
	}
	public void print() {
        for (int i = 0; i < mat.getrow(); i++) {
            for (int j = 0; j < mat.getcol(); j++) 
                System.out.printf("%9.4f ", mat.data[i][j]);
            System.out.println();
        }
    }
	public static void main(String[] args) {
		double[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Matrix bree = new Matrix(m);
		PrintingComp p = new PrintingComp(bree);
		p.print();
	}
}
