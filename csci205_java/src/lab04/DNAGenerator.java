package lab04;

import java.util.Arrays;

public class DNAGenerator {
	String[] rand = new String[1000];
	double[] prob = new double[4];

	public DNAGenerator(String[] a, double[] b) {
		rand = a;
		prob = b;
	}

	public String[] getArray() {
		return rand;
	}

	public void findDist(String a) {
		double totalA = 0.0;
		double totalT = 0.0;
		double totalG = 0.0;
		double totalC = 0.0;
		int i = 0;
		
		while (i < 1000) {
			if (a.charAt(i) == 'A')
				totalA += 1;
			else if (a.charAt(i) == 'T')
				totalT += 1;
			else if (a.charAt(i) == 'G')
				totalG += 1;
			else if (a.charAt(i) == 'C')
				totalC += 1;
			i++;
		}
		System.out.println(3 * totalA / 10.0 + "% A");
		System.out.println(3 * totalT / 10.0 + "% T");
		System.out.println(3 * totalG / 10.0 + "% G");
		System.out.println(3 * totalC / 10.0 + "% C");
		
	}

	public String gen() {
		int i = 0;
		
		while (i < 1000) {
			double r = Math.random();
			if (r < prob[0])
				rand[i] = "A";
			else if (r < (prob[0] + prob[1]))
				rand[i] = "T";
			else if (r < (prob[0] + prob[1] + prob[2]))
				rand[i] = "G";
			else
				rand[i] = "C";
			i++;
		}
		return Arrays.toString(rand);
	}

	public static void main(String[] args) {

		String[] a = new String[1000];
		double[] b = {.05, .25, .15, .55};
		DNAGenerator r = new DNAGenerator(a, b);
		System.out.println(r.gen());
		r.findDist(r.gen());

	}

}
