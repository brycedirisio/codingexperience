package lab04;

public class loadedDice {
	private double dieroll;
	
    // double in the range [0.0, 1.0)
	public loadedDice()
	{
		dieroll = Math.random();
	}
	public double get_Probability()
	{
		return dieroll;
	}
	public void set_Probability(double r)
	{
		dieroll = r;
	}
	public int roll_dice()
	{
        // integer in the range 1 to 6 with desired probabilities
        int roll;
        if      (dieroll < 1.0/8.0) roll = 1;
        else if (dieroll < 2.0/8.0) roll = 2;
        else if (dieroll < 3.0/8.0) roll = 3;
        else if (dieroll < 4.0/8.0) roll = 4;
        else if (dieroll < 5.0/8.0) roll = 5;
        else                  roll = 6;
        
        // print result
        return roll;
	}

	public static void main(String[ ] args) {
		loadedDice bd = new loadedDice();
		int roll = bd.roll_dice();
		System.out.println(roll);
		
	}
}

