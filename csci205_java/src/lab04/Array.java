package lab04;
	
public class Array {

	int len;
	double[] a;
	double[] b;
	
	public Array (double[] list) {
		a = list;
		len = a.length;
	}

	public void create() {
	    // initialize to random values between 0 and 1	    
	    for (int i = 0; i < len; i++) {
	        a[i] = Math.random();
	    }
	}

	public void printArray() {
        // print array values, one per line
        System.out.println("a[]");
        System.out.println("-------------------");
        for (int i = 0; i < len; i++) {
            System.out.println(a[i]);
        }
        System.out.println("-------------------");
	}

	public void findMax() {
        // find the maximum
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < len; i++) {
            if (a[i] > max) max = a[i];
        }
        System.out.println("max = " + max);
	}
	
	public void findMin() {
        // find the maximum
        double min = Double.POSITIVE_INFINITY;
        for (int i = 0; i < len; i++) {
            if (a[i] < min) min = a[i];
        }
        System.out.println("min = " + min);
	}
	
	public void sumArray() {
		double s = 0;
		for (int i = 0; i < a.length; i++) {
			s += a[i];
		}
		System.out.println("sum = " + s);
	}

	public double average() {
        // average
        double sum = 0.0;
        for (int i = 0; i < len; i++) {
            sum += a[i];
        }
        System.out.println("average = " + sum / len);
        return sum / len;
	}
	
	public double[] copyArray() {
	       // copy to another array
        double[] b = new double[len];
        for (int i = 0; i < len; i++) {
            b[i] = a[i];
        }
        return b;
	}
	
	public void reverse() {
        // reverse the order
        for (int i = 0; i < len/2; i++) {
            double temp = b[i];
            b[i] = b[len-i-1];
            b[len-i-1] = temp;
        }
	}

	
	public void dotProducts() {
        // dot product of a[] and b[]
        double dotProduct = 0.0;
        double[] b = copyArray();
        for (int i = 0; i < len; i++) {
            dotProduct += a[i] * b[i];
        }
        System.out.println("dot product of a[] and b[] = " + dotProduct);
    }	
	
	public static void main(String[] args) {
		double[] a = new double[10];
        Array a1 = new Array(a);
        a1.create();
        a1.printArray();
        a1.findMax();
        a1.findMin();
        a1.sumArray();
        a1.average();        
        a1.dotProducts();
	}
}