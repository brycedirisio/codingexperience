package lab04;

public class Switch {
	int num1;
	int num2;
	String oper;
	
	public Switch(int a , int b, String c) {
		num1 = a;
		num2 = b;
		oper = c;
	}
	
	public void theSwitch() {
		int result = 0;
		switch (oper) {
			case "+":
				result = num1 + num2;
				break;
			case "-":
				result = num1 - num2;
				break;
			case "*":
				result = num1 * num2;
				break;
			case "/":
				result = num1 / num2;
				break;
			default:
				System.out.println("There is an error in the input.");
			}
		System.out.println(result);
	}

	public static void main(String[] args) {
		Switch s = new Switch(3, 4, "+");
		s.theSwitch();

	}
}
