package lab04;

public class NewtonSqrt {
	private double x0 ;
	private double tolerance ;
	// constructor
	public NewtonSqrt ( double i , double d ) {
		x0 = i; 
		tolerance = d ;
	}
	public double findSqrt () {
		double xn = x0 ;
		while ( Math.abs ( xn - (( xn + x0 / xn ) /2) ) >
		tolerance * xn ) {
			xn = ( xn + x0 / xn ) /2 ;
		}
		return xn ;
	}
	
	public static void main ( String [] args ) {
		double num = 152;
		NewtonSqrt n1 = new NewtonSqrt ( num ,1e-15);
		System.out.println( n1.findSqrt());
		System.out.println( Math.sqrt(num));
	}
}