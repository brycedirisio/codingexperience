package lab08;

public class RectangleRule {

	   /**********************************************************************
	    * Integrate f from a to b using the rectangle rule.
	    * Increase n for more precision.
	    **********************************************************************/
	    public static double integrate(Function f, double a, double b, int n) {
	        double delta = (b - a) / n;           // step size
	        double sum = 0.0;                     // area
	        for (int i = 0; i < n; i++) {
	            sum += delta * f.evaluate(a + delta*(i + 0.5));
	        }
	        return sum;
	    }


	 


}
