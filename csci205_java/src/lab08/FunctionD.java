package lab08;

public class FunctionD implements Function{
	   
    public double evaluate(double x) {
        return (Math.sin(x) * Math.sin(x)) + (Math.cos(x) * Math.cos(x));
    }
}
