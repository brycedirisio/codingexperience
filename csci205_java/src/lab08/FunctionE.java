package lab08;

public class FunctionE implements Function{
	   
    public double evaluate(double x) {
        return Math.log(1 + x);
    }
}

