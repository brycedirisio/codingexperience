package lab08;

public class FunctionB implements Function{
	   
    public double evaluate(double x) {
        return Math.exp(x) + Math.sqrt(x);
    }
}
