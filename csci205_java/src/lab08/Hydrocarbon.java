package lab08;

public enum Hydrocarbon {
	OCTANE("Octane", 8, 18), OCTENE("Octene", 8, 16), OCTYNE("Octyne", 8, 14), CYCLOOcTANE("Cyclooctane", 8, 16), DECANE("Decane", 10, 22), DECENE("Decene", 10, 20), DECYNE("Decyne", 10, 18), CYCLODECANE("Cyclodecane", 10, 20);

	private final int carbon;
	private final int hydrogen;
	private final String name;
	private double cMass = 12.01;
	private double hMass = 1.008;

	// constructor
	Hydrocarbon(String n, int c, int h) {
		this.carbon = c;
		this.hydrogen = h;
		this.name = n;

	}
	
	public double atomicWeight() {
		return (double)(cMass*carbon) + (hMass * hydrogen);
	}
	
	public String getName() {
		return name;
	}
	
	public static void main(String [] args) {
		//Hydrocarbon hc = new Hydrocarbon();
        for (Hydrocarbon hc : Hydrocarbon.values())
            System.out.printf("Atomic weight of " + hc.name + " is: " + hc.atomicWeight()+ "\n");
        	
	}


}
