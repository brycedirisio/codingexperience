package latex;

public class Cylinder extends Circle {
	   // private instance variable
	   private double height;
	   
	   // Constructors
	   public Cylinder() {
	      super();  // invoke superclass' constructor Circle()
	      this.height = 1.0;
	   }
	   public Cylinder(double height) {
	      super();  // invoke superclass' constructor Circle()
	      this.height = height;
	   }
	   public Cylinder(double height, double radius) {
	      super(radius);  // invoke superclass' constructor Circle(radius)
	      this.height = height;
	   }
	   public Cylinder(double height, double radius, String color) {
	      super(radius, color);  // invoke superclass' constructor Circle(radius, color)
	      this.height = height;
	   }
	   
	   // Getter and Setter
	   public double getHeight() {
	      return this.height;
	   }
	   public void setHeight(double height) {
	      this.height = height;
	   }

	   // Return the volume of this Cylinder
	   public double getVolume() {
	      return getArea()*height;   // Use Circle's getArea()
	   }

	   // Describle itself
	   public String toString() {
	      return "This is a Cylinder";  // to be refined later
	   }
	   
		   public static void main(String[] args) {
		      Cylinder cy1 = new Cylinder();
		      System.out.println("Radius is " + cy1.getRadius()
		         + " Height is " + cy1.getHeight()
		         + " Color is " + cy1.getColor()
		         + " Base area is " + cy1.getArea()
		         + " Volume is " + cy1.getVolume());
		   
		      Cylinder cy2 = new Cylinder(5.0, 2.0);
		      System.out.println("Radius is " + cy2.getRadius()
		         + " Height is " + cy2.getHeight()
		         + " Color is " + cy2.getColor()
		         + " Base area is " + cy2.getArea()
		         + " Volume is " + cy2.getVolume());
		   }
		
	}