package latex;

import java.util.ArrayList;

public class CashRegister {
	//List of all items purchased today
	private ArrayList<Double> itemList;
	//Name of the drawer
	private String sName = "";
	
	//Amount of cash in drawer
	private double cashInDrawer;
	
	//Are we in a transaction currently
	private boolean isInTransaction;
	
	//Total in current transaction
	private double transTotal;
	
	//Number of items in current transaction
	private int numItemsInTrans = 0;
	
	//Amount paid in current transaction
	private double amountPaid;
	
	/**No paramteter constructor**/
	public CashRegister()
	{
		this.itemList = new ArrayList<Double>();
		this.sName = "Default";
		this.cashInDrawer = 0;
		this.isInTransaction = false;
		this.transTotal = 0;
		this.numItemsInTrans = 0;
		this.amountPaid = 0;
	}
	
	public CashRegister(String name)
	{
		this();
		this.sName = name;
	}
	
/**************************************************************************************************
 * Accessor methods
 */
	public double getTransactionTotal()
	{
		return this.transTotal;
	}
	
	public int getNumItemsInTrans()
	{
		return this.numItemsInTrans;
	}
	
	public double getAmountPaid()
	{
		return this.amountPaid;
	}
	
	public String getName()
	{
		return this.sName;
	}
	
	public boolean isInTransaction()
	{
		return this.isInTransaction;
	}
	
	public double getAmountOwed()
	{
		return this.transTotal - this.amountPaid;
	}

	
/**************************************************************************************************
 * Mutator methods
 */
	
	public void setName(String name)
	{
		this.sName = name;
	}
/**************************************************************************************************
 * Functional methods
 */
	
	/**
	 * This method will begin the current working day
	 * @param init is the amount of money to start the register.
	 */
	public void startDay(double init)
	{
		this.itemList.clear();
		this.cashInDrawer = init;
	}
	
	/**
	 * Finish day will return the cash in the register and will set it to zero
	 * @return temp is the cash in the drawer at the end of the working day
	 */
	public double finishDay()
	{
		double temp = this.cashInDrawer;
		this.cashInDrawer=0;
		return temp;
	}
	
	/**
	 * This method will start a transaction unless there is already one started
	 * @return true or false based on weather or not a new transaction is started
	 */
	public boolean startTransaction()
	{
		if(!this.isInTransaction && this.cashInDrawer !=0)
		{
			this.isInTransaction = true;
			return this.isInTransaction;
		}
		else
			return false;
	}
	
	/**
	 * Returns true if the transaction is closed and false if it is not
	 * @return true or false depending on if the transaction is closable
	 */
	public boolean finishTransaction()
	{
		if(this.isInTransaction && this.getAmountOwed() == 0)
		{
			this.isInTransaction = false;
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Increases total cost of the transaction and number of items in cart
	 * @param price a double representing the price of the item to buy
	 */
	public void scanItem(double price)
	{
		this.transTotal +=price;
		this.numItemsInTrans++;
		this.itemList.add(price);
	}
	
	/**
	 * This method will allow the register to collect payment on the current transaction
	 * @param payment is the amount the customer is paying
	 * @returns the amount the customer owes or amount owed to the customer
	 */
	public double collectPayment(double payment)
	{
		this.amountPaid +=payment;
		this.cashInDrawer +=payment;
		
		return this.amountPaid-this.transTotal;
	}
	
	
	/**
	 * This method will return the number of items purchased during the day
	 * @returns the number of items purchased today
	 */
	public int getNumTransToday()
	{
		return this.itemList.size();
	}
	
	
	/**
	 * This method will return the average price of all items purchased today
	 * @return average of items purchased today
	 */
	public double getAveTransToday()
	{
		double ave = 0;
		for(int i = 0; i < this.itemList.size();i++)
			ave+=this.itemList.get(i);
		
		ave/=this.itemList.size();
		
		return ave;
	}
	
	public void displayItemList()
	{
		System.out.println("\nTotal items purchased: "+this.getNumTransToday()+"\nPrices of the items");
		for(int i = 0; i < this.getNumTransToday(); i++)
			System.out.println(this.itemList.get(i));
	}
	
	public String toString()
	{
		String temp= "NOT IN TRANSACTION";
		if(this.isInTransaction())
			temp = "IN TRANSACTION";
		return this.sName +":\tDrawer: "+this.cashInDrawer+"\t"+temp+"\tNumber of items today: " +this.getNumTransToday();
	}
	
/*************************************************************************************************
 * Main method
 */
public static void main(String[]args)
{
	 CashRegister reg1 = new CashRegister(), reg2 = new CashRegister(
			 "Register 2");
			 System.out.println("Constructed:\n" + reg1 + "\n" + reg2);
			 reg1.setName("Register 1");
			 reg1.startDay(100.0);
			 reg2.startDay(200.0);
			 System.out.println("Started day:\n" + reg1 + "\n" + reg2);
			 System.out.println("TESTING: reg1: New transaction: $2.50, $9.95, $5.50 = $17.95");
			 reg1.startTransaction();
			 reg1.scanItem(2.50);
			 reg1.scanItem(9.95);
			 reg1.scanItem(5.50);
			 System.out.println("reg1.isInTransaction() = true. ACTUAL = " + reg1.isInTransaction() );
			 System.out.printf("reg1.getTotal() = $17.95. ACTUAL = $%.2f%n",
			reg1.getTransactionTotal());
			 System.out.println("reg1.getNumItems() = 3. ACTUAL = " + reg1.getNumItemsInTrans());
			 System.out.println("reg1.getAmountPaid = 0.0 ACTUAL = " + reg1.getAmountPaid());
			 System.out.println("reg1.getAmountOwed() = 17.95. ACTUAL = " + reg1.getAmountOwed());
			 System.out.println("Testing out toString() for reg1...");
			 System.out.println(reg1);
			 double amtBack = reg1.collectPayment(15.0);
			 System.out.printf("reg1: made payment of $15. Result = -2.95: ACTUAL = $%.2f%n", amtBack);
			 System.out.println(reg1);
			 System.out.println("reg1: reg1.finishTransaction() = false. ACTUAL ="+reg1.finishTransaction());
			 System.out.printf("reg1: reg1.collectPayment(10), result: $7.05 ACTUAL = $%.2f%n",
			reg1.collectPayment(10));
			 System.out.println(reg1);
			 System.out.println("reg1: reg1.finishTransaction() = true. ACTUAL ="+reg1.finishTransaction());
			 System.out.println(reg1);
			 System.out.println("reg1: New transaction: $10, $7.50, $19.95, $5 = $42.45");
			 reg1.startTransaction();
			 reg1.scanItem(10);
			 reg1.scanItem(7.50);
			 reg1.scanItem(19.95);
			 reg1.scanItem(5.00);
			 System.out.println(reg1);
			 System.out.printf("reg1.collectPayment($42.45) = 0.0. ACTUAL = $%.2f%n",
			reg1.collectPayment(42.45));
			 System.out.println(reg1);
			 reg1.finishTransaction();
			 System.out.println("Checking reg2, should still be untouched:");
			 System.out.println(reg2);
			 System.out.printf("reg1.finishDay() = $160.40. ACTUAL = $%.2f%n", reg1.finishDay());
			 System.out.println("Reg1 should be empty: " + reg1);
			 System.out.printf("reg2.finishDay(): = $200. ACTUAL = $%.2f%n", reg2.finishDay());
			 System.out.println("Reg2 shuold be empty: " + reg2);
}
	

}
