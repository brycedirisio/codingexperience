package lab03;

public class Function {
	private double x;
	private double answer; 
	
	public Function(double a) {
		x = a;
	}
	
	public Exp exp() {
		Exp exp = new Exp(x);
		return exp;
	}
	
	public Cos cos() {
		Cos cos = new Cos(x);
		return cos;
	}
	
	public Sin sin() {
		Sin sin = new Sin(x);
		return sin;
	}

	public double calculate() {
		answer = (sin().calculate() + 2*cos().calculate())/exp().calculate();
		return answer;
	}
	
    public static void main(String[] args) { 
        double x = 4; //Double.parseDouble(args[0])
        Function func = new Function(x);
        System.out.println(func.calculate());
        
        Sin sin = new Sin(x);
        double a = sin.calculate(); 
        Cos cos = new Cos(x);
        double b = cos.calculate(); 
        Exp exp = new Exp(x);
        double c = exp.calculate(); 
        
    }

}

