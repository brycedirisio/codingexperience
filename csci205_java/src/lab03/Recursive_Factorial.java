package lab03;

public class Recursive_Factorial {
	private long num;

	public Recursive_Factorial(long n) {
		num = n;
	}
	
    public long recFactorial(long num) {
		if (num == 0) return 1;
		else return num * recFactorial(num-1);
    }
    
    public static void main(String[] args) {
        long n = 21;//Long.parseLong(args[0]);\
        Recursive_Factorial fac = new Recursive_Factorial(n);
        System.out.println(fac.recFactorial(n));
    }
}
        


