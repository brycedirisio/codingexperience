package lab03;

public class Sin {
	private double num;
	
	public Sin(double a) {
		num = a;
	}
	
	public double calculate() {

        num = num % (2 * Math.PI);

        double term = 1.0;      
        double sum  = 0.0;      

        for (int i = 1; term != 0.0; i++) {
            term *= (num / i);
            if (i % 4 == 1) sum += term;
            if (i % 4 == 3) sum -= term;
        }
            
        return sum;
		
	}
    public static void main(String[] args) { 
        double x = Math.PI/6; 
        Sin num = new Sin(x);
        System.out.println(num.calculate());
        System.out.println("\n");
        System.out.println(Math.sin(x));
    }
}
