package lab03;

public class Stirling_Factorial {
	private long num;
	private long result;

	public Stirling_Factorial(long n) {
		num = n;
	}
	
	public long stirFactorial() {
		result = new Double (Math.sqrt(2*Math.PI*num) * Math.pow((num/Math.E), num)).longValue();
		return result;
    }
    
    public static void main(String[] args) {
        long n = 21;
        Stirling_Factorial fac = new Stirling_Factorial(n);
        System.out.println(fac.stirFactorial());
    }

}
