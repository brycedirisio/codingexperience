package lab03;


public class Exp {
	private double num;
	private boolean isNegative;
	
	public Exp(double a) {
		num = a;
	}
	
	public double calculate() {

        // compute e^x assuming x >= 0
        double term = 1.0;
        double sum = 0.0;
        for (int n = 1; sum != sum + term; n++) {
            sum += term;
            term *= num/n;
        }
            // print results
        if (isNegative) {
        		sum = 1.0 / sum;
        }
        //System.out.println(sum);
        return sum;
        

        
	}
	
	public void isNegative() {
		isNegative = false;
		if (num < 0) {
			isNegative = true;
			num = -num;
		}
		
	}
    public static void main(String[] args) { 
        double x = 4; //Double.parseDouble(args[0]);
        Exp num = new Exp(x);
        num.isNegative();
        System.out.println(num.calculate());
        //System.out.println("\n");
        //System.out.println(Math.exp(x));
        

     
    }
}
