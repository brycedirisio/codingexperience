package lab03;

public class Factorial {
	private long num;
	
	public Factorial(long a) {
		num = a;
	}
	
	public Recursive_Factorial rec() {
		Recursive_Factorial rec = new Recursive_Factorial(num);
		return rec;
	}
	
	public Stirling_Factorial stir() {
		Stirling_Factorial stir = new Stirling_Factorial(num);
		return stir;
	}
	
    
    public void choose() {
    		if (num <  0) throw new RuntimeException("Underflow error in factorial");
        else if (num < 21) System.out.println(rec().recFactorial(num));
        else System.out.println(stir().stirFactorial());
        System.out.println("\n");
    }
     

    public static void main(String[] args) {
        long n = 21;
        Factorial fac = new Factorial(n);
        fac.choose();
        
        
        	
        
       
    }

}
