package lab03;

 public class Word_Array {
	private String str;
	
	public Word_Array(String a){
		str = a;
	}
	
	public String[] getArray ( String str ) {
		String[] ary = str.split ("") ;
		return ary;
	}
	
	public void print_array ( String [] s ) {
		for (int i = 0; i < s.length; i++)
		System.out.println(s[i]);
	}
	
	public void vowelCount() {
		String withoutVowels = str.toLowerCase().replaceAll("a|e|i|o|u|", "");
		int d1 = str.length();
		int d2 = withoutVowels.length();
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d1-d2);
		}
	
	public void reverse() {
		String[] temp = getArray(str);
		String firstLast = temp[0];
		temp[0] = temp[temp.length - 1];
		temp[temp.length - 1] = firstLast;
		print_array(temp);
		
	}
	
	public boolean is_palindrome() {
		String[] temp = getArray(str);
		int max = temp.length - 1;
		int first = 0;

		while (first < max) {
			if (temp[first].equals(temp[max]) == false) {
				return false;	
			}
			
			first++;
			max--;
		}
		return true;
	}
	
	public void test() {
		this.print_array(this.getArray(str));
		System.out.println("\n");
		this.vowelCount();
		System.out.println("\nReversed:");
		this.reverse();
		System.out.println("\nIs it a palindrome?\n");
		if (this.is_palindrome() == false) {
			System.out.println("No");
			
		}
		if (this.is_palindrome() == true) {
				System.out.println("Yes");
				
		}
			
	}
	
	public static void main (String[] args) {
		String s = "aossoa";
		Word_Array a = new Word_Array(s);
		a.test();
	}
}
