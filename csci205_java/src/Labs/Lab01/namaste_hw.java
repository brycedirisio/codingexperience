//Bryce DiRisio
//CSCI 205, Lab 01
//namaste_hw.java

package namaste_as_an_object;

public class namaste_hw 
{
private String str;

//constructor

	public namaste_hw(String string) 
	{
		str=string;
	}
//args
	
	public static void main( String[] args)
	{
		System.out.print("Namaste, Lab01!\n");
		System.out.print("How are you?\n");
		System.out.print("Life is meaningless.\n");
		System.out.print("OK have a nice day!\n");
		
	}

//getStr method
	public String getStr() {
		return str;
	}
//setStr method
	public void setStr(String str) {
		this.str = str;
	}
}