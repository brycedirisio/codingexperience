
package lab06;

import java.io.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class latexit1 {	
private String address;
private String file_name;
private String prog;




public latexit1 (String a1, String f1, String p1) throws Exception{
	address=a1;
	file_name=f1;
	prog = p1;	
}



public void run_this_file() throws IOException{
	String add="\""+address+"\"";
	String path_to_file= "cd "+add+" && "+prog+" "+file_name ;
	System.out.println(path_to_file);
	String[] run_it ={"cmd.exe", "/c", path_to_file};
	//String[] run_it ={"/bin/bash", "-c", path_to_file};
	ProcessBuilder builder = new ProcessBuilder(run_it);
	builder.redirectErrorStream(true);
    Process p = builder.start();
    BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
    String line;
    while (true) {
        line = r.readLine();
        if (line == null) { break; }
        System.out.println(line);
    }

}


public static void main(String[] args) throws Exception {
	/*
	string a1 is the address of the file location
	f1 is name of the latex file we run
	and p1 is the program. Apple users ask me for help.
	
*/
	String a1= "C:\\Users\\ryangiallonardo\\Documents\\Bucknell 2017-2018\\Second Semester\\CSCI205\\Lab06";
	String f1= "runthis(1)";
	String p1= "pdflatex";
//which pdflatex
	latexit1 r2= new latexit1(a1,f1,p1);
	r2.run_this_file();
	
	}
}
