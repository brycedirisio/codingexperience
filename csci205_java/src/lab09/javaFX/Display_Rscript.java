package javaFX;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Display_Rscript extends JFrame {
	private String address;
	private String file_name;
	private String prog;

/**
	 * reading an object requires serialization
*/
	private static final long serialVersionUID = 5895649599564936819L;

public Display_Rscript(String a, String f, String p) {
	address = a;
	file_name = f;
	prog = p;
}

public void run_this_file() throws IOException{
	String add="\""+address+"\"";
	String path_to_file= "cd "+add+" && "+prog+" "+file_name ;
	System.out.println(path_to_file);
	String[] run_it ={"cmd.exe", "/c", path_to_file};
	//String[] run_it ={"/bin/bash", "-c", path_to_file};
	ProcessBuilder builder = new ProcessBuilder(run_it);
	builder.redirectErrorStream(true);
    Process p = builder.start();
    BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
    String line;
    while (true) {
        line = r.readLine();
        if (line == null) { break; }
        System.out.println(line);
    }

	  this.setTitle("Network Analysis");
	  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  JPanel panel1 = new JPanel();
	  ImageIcon pic = new ImageIcon("C:/Users/ryangiallonardo/Documents/Bucknell_2017-2018/SecondSemester/CSCI205/Lab09/output2.jpeg");
	  panel1.add(new JLabel(pic));
	  this.add(panel1);
	  this.pack();
	  this.setVisible(true);
  }

public static void main(String[] args) throws IOException {
	/*
	string a1 is the address of the file location
	f1 is name of the latex file we run
	and p1 is the program. Apple users ask me for help.
	
*/
	String a= "C:/Users/ryangiallonardo/Documents/Bucknell_2017-2018/SecondSemester/CSCI205/Lab09";
	String f= "Rscript.R";
	String p= "Rscript";
//which pdflatex
	Display_Rscript r2= new Display_Rscript(a,f,p);
	r2.run_this_file();
	
}
}