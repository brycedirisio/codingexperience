package javaFX;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Display_UML_Class extends JFrame {

/**
	 * reading an object requires serialization
*/
	private static final long serialVersionUID = 5895649599564936819L;
	

public static void main(String[] args) {
    new Display_UML_Class();
  }

  public Display_UML_Class() {
    this.setTitle("Class Diagram");
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JPanel panel1 = new JPanel();
    ImageIcon pic = new ImageIcon("C:/Users/ryangiallonardo/Documents/Bucknell_2017-2018/SecondSemester/CSCI205/Lab09/file.png");
    panel1.add(new JLabel(pic));
    this.add(panel1);
    this.pack();
    this.setVisible(true);
  }
}