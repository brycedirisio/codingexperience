package javaFX;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DisplayImage extends JFrame {
 


/**
	 * reading an object requires serialization
	 */
	private static final long serialVersionUID = 5895649599564936819L;
	

public static void main(String[] args) {
    new DisplayImage();
  }

  public DisplayImage() {
    this.setTitle("Image");
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JPanel panel1 = new JPanel();
    ImageIcon pic = new ImageIcon("C:\\Users\\micha\\eclipse-workspace\\lab9\\src\\.DramaticQuestionMark.png");
    panel1.add(new JLabel(pic));
    this.add(panel1);
    this.pack();
    this.setVisible(true);
  }
}